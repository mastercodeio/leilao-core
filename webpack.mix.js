const path = require('path');
let mix = require('laravel-mix');
const tailwindcss = require('tailwindcss');

mix.webpackConfig({
    resolve: {
        alias: {
            "@common": path.resolve(__dirname, "resources/assets/common"),
            "@site": path.resolve(__dirname, "resources/assets/site"),
            "@system": path.resolve(__dirname, "resources/assets/system"),
            "@tenant": path.resolve(__dirname, "resources/assets/tenant"),
        }
    }
});

/*
 |--------------------------------------------------------------------------
 | Mix Asset Management
 |--------------------------------------------------------------------------
 |
 | Mix provides a clean, fluent API for defining some Webpack build steps
 | for your Laravel application. By default, we are compiling the Sass
 | file for the application as well as bundling up all the JS files.
 |
 */

const directories = ['site', 'system', 'tenant'];

directories.forEach((dir) => {
    const res = 'resources/assets/' + dir;
    const pub = 'public/' + dir;

    mix.js(res + '/js/app.js', pub + '/js')
        .copy(res + '/img', pub + '/img')
        .sass(res + '/sass/app.scss', pub + '/css')
        .options({
            processCssUrls: false,
            postCss: [tailwindcss('tailwind.js')],
        });
});