# MeuLeilão

<p align="center">
<a href="https://travis-ci.org/laravel/framework"><img src="https://travis-ci.org/laravel/framework.svg" alt="Build Status"></a>
<a href="https://packagist.org/packages/laravel/framework"><img src="https://poser.pugx.org/laravel/framework/d/total.svg" alt="Total Downloads"></a>
<a href="https://packagist.org/packages/laravel/framework"><img src="https://poser.pugx.org/laravel/framework/v/stable.svg" alt="Latest Stable Version"></a>
<a href="https://packagist.org/packages/laravel/framework"><img src="https://poser.pugx.org/laravel/framework/license.svg" alt="License"></a>
</p>

## Sobre MeuLeilão

Aplicação para controle de leilões, presenciais e online.

- [Laravel](https://laravel.com/docs)
- [Bulma](https://bulma.io/documentation/overview/start/)


# Guia de desenvolvimento

Este documento contém um conjunto de recomendações / normas / guias utilizados para manter
uma padronização no desenvolvimento da plataforma MeuLeilão. O guia aborda fluxo de trabalho,
estilo de código, e outras pequenas coisas que consideramos importante documentar.

## Pré-requisitos

Você precisa ter instalado:

- Git
- [Docker](https://www.docker.com/)
- [Docker Compose](https://docs.docker.com/compose/)
- [Valet](https://laravel.com/docs/5.6/valet) ou [Valet Linux](https://github.com/cpriego/valet-linux)
- [Node](https://nodejs.org/en/)
- [Yarn](https://yarnpkg.com/en/)

## Instalação

Clone o repositório para uma pasta local:

    $ git clone git@bitbucket.org:mastercodeio/auct-core.git

Duplique o arquivo `*.env.example*`, criando um arquivo `*.env*`, e o arquivo `*.env.testing.example*`,
criando um arquivo `*.env.testing*`. Este arquivo será utilizado quando os testes automatizados forem
executados.

O arquivo `composer.json` estão definidos alguns comandos úteis para utilização do Docker:

```
composer run-script docker-up
composer run-script docker-stop
composer run-script docker
```

Os containers utilizados são:

- postgres: PostgreSQL para banco de dados;
- pgadmin: Administração do banco de dados;
- mailhog: Caixa de entrada para teste dos emails.

Há diversos outros containeres disponíveis na configuração, mas não utilizados ainda.
Caso deseje, é possível utilizar o Docker para executar um servidor Nginx, com PHP já
instalado. Para isso, use `cd docker && docker-compose up -d nginx`. Porém, utilizamos
por padrão o Laravel Valet para esta parte.

**Usando o Valet**

Depois de instalar o Laravel Valet, acesse a pasta do projeto MeuLeilão e execute os comandos:

```sh
valet link auct
valet link sys.auct
valet link auct-client
```

Desta forma, você poderá acessar a aplicação em `http://auct.test` para visualizar o site,
`http://sys.auct.test` para visualizar a área do sistema, e `http://auct-client.test` para
visualizar a área de um cliente (deverá retornar 404 enquanto o cliente não for cadastrado).

**Usando o nginx do Docker**

O arquivo .env.example vem preparado para utilizar o Laravel Valet. Você precisará alterar as variáveis de ambiente
para banco de dados e email no arquivo `.env`. Substitua `127.0.0.1` pelo nome do container. Ex:

```
DB_HOST=postgres
```

### Atenção!

Os comandos do composer e artisan (que interagem com banco de dados) não devem ser executados
diretamente pelo seu sistema de arquivos, e sim dentro do container Docker. Para isso, você
deve executar, primeiramente, o comando:

    $ cd docker
    $ docker-compose exec workspace bash

Você terá acesso ao terminal do container Docker, onde poderá executar os demais comandos.

## Instalando as dependências

Execute o composer para instalar as dependências do projeto.

    composer install

Caso o comando retorne uma lista de extenções do PHP que precisam ser instaladas. Instale
todas que estiverem faltando. Por ex.:

    $ sudo apt-get install php-mbstring
    $ sudo apt-get install php-bcmath
    $ sudo apt-get install php-mysql
    $ sudo apt-get install php-xml

> Após realizar todas as instalações execute novamente o composer install.

Após o composer instalar as dependências, execute os comandos abaixo:

    $ php artisan key:generate
    $ yarn                       // ou npm install se você utiliza npm


**BANCO DE DADOS**

Como o MeuLeilão é uma aplicação _Multi-Tenancy_, alteramos a forma que alguns comandos de migração
do Laravel funcionam. Para os comandos `migrate`, `migrate:fresh` e `migrate:rollback`, adicionamos
as opções `--all` e `--schema=`. Ex:

```sh
# Para executar a migração para os clientes auth-client.test e meusleiloes.com.br
php artisan migrate --schema=auct-client.test,meusleiloes.com.br

# Para executar a migração no schema do Sistema
php artisan migrate

# Para executar a migração nos schemas de todos os clientes
php artisan migrate --all
```

# Controle de versão

Considere que para todos os repositórios da MeuLeilão, **a branch master sempre deve estar
estável**. Isso significa que deve ser seguro e possível realizar deploy da branch *master*
a qualquer momento.

Todas as branches existentes no repositório são consideradas ativas (sendo usadas para
desenvolvimento). A partir do momento que a branch não é mais utilizada (por ex. seu merge
foi realizado), a branch deve ser removida.

O desenvolvimento **não deve ser realizado na branch master**. A branch **develop**
deve receber todas as atualizações de código antes. A branch **develop** recebe as alterações
de código para serem enviadas ao servidor de homologação. Esta etapa é necessária e obrigatória
antes do código ir para **master**. Logo, tudo o que for para **develop** automaticamente
será considerado apto para testes.

**FEATURE BRANCHES**

*Feature branches* são utilizadas para implementação de funcionalidades ou bugs. Não há uma
regra restrita sobre o nome das branches, apenas tenha certeza que o nome é claro o suficiente
para explicar o objetivo da branch:

**RUIM:** lance, ajustes

**BOM:** feature/lance-online, bugfix/fechamento-leilao

**PULL REQUESTS**

Assim que a branch de feature ou bug for finalizada, abra um *pull requests* para que o
administrador do repositório faça um **merge** para a branch `develop`. Esse processo,
além de mais seguro, também é legal para alguém revisar seu código e a equipe discutir
melhorias.

**MERGE E REBASE**

O ideal é que você faça rebase da sua branch regularmente para reduzir a chance de
conflitos de merge.

- Se você deseja fazer merge entre duas branches, use `git merge <branch> --squash`
- Se ocorreu erro no seu push, primeiro faça o rebase da sua branch com `git rebase`

## Commits

Apesar de não haver regra restrita sobre seus commits, mensagens descritivas são recomendadas.
O ideal é que a mensagem deixe claro o que foi feito no commit.

**RUIM:** wip, bastante coisa, up, !!!, att

**BOM:** Atualiza as dependências do Laravel, Corrige o cálculo de incrementos de lances

## Dicas

- Merge vs. rebase on [Atlassian](https://www.atlassian.com/git/tutorials/merging-vs-rebasing/workflow-walkthrough)
- Merge vs. rebase by [@porteneuve](https://medium.com/@porteneuve/getting-solid-at-git-rebase-vs-merge-4fa1a48c53aa)

# Guia de estilo - Laravel

Primeiramente, você consegue obter mais valor do Laravel quando desenvolve as coisas do jeito
Laravel de desenvolver. Se há algum meio documentado de fazer algo, siga-o. Se você precisa fazer
algo de forma diferente, tenha certeza que você tem algum motivo para não ter seguido o padrão.

## IDE / Editor

Recomenda-se utilizar uma IDE (PHPStorm!) para trabalhar com o código PHP. Apesar de editores
serem legais (VSCode), a PHPStorm facilita a vida e proporciona ganhos enormes.

De qualquer forma, busque utilizar o editor que:

- Suporte `.editorconfig`;
- Adicione os namespaces (ex: `use Awesome\Namespaces`) automaticamente quando você utiliza
uma classe no seu código;
- Indique erros e variáveis não utilizadas no seu código (pode ser usando phplint, jslint…).

## Regras gerais do PHP

O estilo de código deve seguir os padrões [PSR-1](http://www.php-fig.org/psr/psr-1/) e
[PSR-2](http://www.php-fig.org/psr/psr-2/). Se não conhece ou tem dúvidas, leia os guias dos
links.

## Laravel - Melhores Práticas

Um link útil é o [Laravel: Best Practices](http://www.laravelbestpractices.com/). No site são
apresentadas melhores práticas para o desenvolvimento com o Laravel Framework.

## Clean Code

Escreva código que os outros possam ler: leia sobre [Clean Code para PHP](https://github.com/jupeter/clean-code-php).

### Condicionais - Ifs+Else / Switches

Condicionais else e switch são totalmente desnecessárias (*Clean Code*). Se você está utilizando-os, repense seu código. Para eliminá-los, você pode utilizar *returns* para interromper a execução do código, condições ternárias, ou mesmo extração de trechos para métodos.

```php
//
// ERRADO !!!!
//

public function authorizeTransaction(User $user, Transaction $transaction)
{
  $error = null;
  
  if ($user->status === 'approved' && $transaction->status === 'pending') {
    $transaction->update(['status' => 'approved']);
  } elseif ($user->status === 'approved' && $transaction->status === 'incomplete') {
    $error = 'Transação pendente';
  } elseif ($user->status === 'blocked') {
    $error = 'Usuário bloqueado!';
  } else {
    $error = 'Erro desconhecido!';
  }
  
  return $error;
}

//
// CERTO !!!!
//

public function authorizeTransaction(User $user, Transaction $transaction)
{
  if ($user->status === 'blocked') {
    return 'Usuário bloqueado!';
  }
    
  if ($transaction->status === 'incomplete') {
    return 'Transação incompleta!';
  }
  
  if ($user->status !== 'approved' || $transaction->status !== 'pending') {
    return 'Erro desconhecido';
  }
  
    $transaction->update(['status' => 'approved']);
    return null;
}
```

### Configuração

Arquivos de configuração devem utilizar kebab-case.

    config/
      pdf-generator.php

Chaves de configuração devem utilizar snake_case.

    return [
      'chrome-path': env('CHROME_PATH'),
    ];

Não utilize o helper `env` fora dos arquivos de configuração. Crie um valor de configuração
a partir de uma variável de ambiente como exibido acima.

**Obs:** O Laravel já vem com vários arquivos de configuração. Um em especial, `services.php`,
pode ser utilizado para você adicionar configurações de serviços de terceiros, como Google Maps.

### Comandos Artisan

O nome dos comandos artisan devem ser todos kebab-case e prefixados com `auct:` (se desenvolvidos
especificamente para os projetos da MeuLeilão).

    # BOM
    php artisan auct:backup-media-files
    
    # RUIM
    php artisan auct_BackupMediaFiles

Um comando sempre deve fornecer algum feedback sobre as operações realizadas. O mínimo aceitável
é que o comando retorne um comentário indicando que tudo ocorreu bem. Ah! Utilize mensagens descritivas.

    public function handle()
    {
      // ...
      
      $this->comment('O backup de arquivos de mídia foi concluído, e pode ser encontrado em https://meusbackups.com.br/abc123');
    }

## Rotas

Rotas devem utilizar kebab-case (O Google inclusive as entende melhor!).

    https://www.auct.com.br/termos-de-uso

O nome das rotas devem ser em inglês e utilizar kebab-case. Os links devem utilizar o helper `route`.


    Route::get('termos-de-uso', 'UseTermsController@index')->name('use-terms');
    
    # Blade
    
    <a href="{{ route('use-terms') }}">Termos de Uso</a>

Todas as rotas possuem um verbo HTTP. Ao definir as rotas, declare-os antes dos demais parâmetros para facilitar a leitura.

    Route::get('/', 'HomeController@index')->name('home')->middleware('guest');

## Controllers

Controllers que controlam um recurso devem utilizar nome do recurso no singular. O próprio
Laravel cria comandos no singular quando você gera um Model com o parâmetro de controller.


    class ClientProfileController
    {
      // ...
    }

O ideal é que os controllers sejam simples e mantenham as funções padrões do (`index`, `create`,
`store`, `show`, `edit`, `update`, `destroy`). Crie um novo controller se você precisa de
outras ações.

O Laravel permite fazer *binding* de Models nos Controllers. O ideal é que você siga esta
maneira de obter os registros dos bancos de dados. Lembre-se que você pode criar novas regras
de *binding*, por exemplo para que o binding considere apenas usuários de um determinado
tipo. Leia mais na [documentação do Laravel](https://laravel.com/docs/5.6/routing#route-model-binding).

> Para manter os Controllers enxutos, utilize classes de Serviços para realizar operações.

## Validação

Regras de validação personalizadas devem ser criadas através do comando `php artisan make:rule`.

## Autorização

Policies e Gate devem utilizar kebab-case:

    Gate::define('edit-client-profile', function ($user, $clientProfile) {
      return $user->client_profile_id === $clientProfile->id;
    });

    @can('edit-client-profile', $clientProfile)
      <a href="{{ route('client-profiles.edit', $clientProfile) }}">Editar Perfil</a>
    @endcan

## Comentários

Comentários devem ser úteis. Não exagere neles.

    /**
     * Calcula os custos de delivery para a transação.
     *
     * @param App\Models\Transaction $transaction
     * @return float
     */
    protected function calculateDeliveryCosts($transaction): float
    {
      // Comentários de uma única linha podem ser utilizados para pequenas explicações.
      
      // Pode ser que você precise explicar bastante coisa.
      // Normalmente são os casos em que comentários são de fato úteis.
      // Ah! Não exagere no tamanho das linhas, dê preferência para
      // linhas curtas, no máximo 80 caracteres. 
    }

## Classes

Dê bons nomes para suas classes - e em inglês!

Evite classes muito extensas (+ de 100 linhas).  Se sua classe é muito extensa, é sinal de
que ela pode ser particionada para outras classes. Se você está usando Ctrl-C / Ctrl-V em
métodos e pedaços de código, também é sinal que você pode criar classes, métodos reutilizáveis,
traits ou classes abstratas.

Sempre que possível declare os tipos de parâmetros aceitos e o que os métodos retornam:

    class TransactionsHandler
    {
      //...
      
      public function calculateDeliveryCosts(Transaction $transaction): float
      {
        // ...
        return 4.5;
      }
    }

# Design Patterns

## SOLID

O termo SOLID define cinco princípios de *design* destinados a promover desenvolvimento de
software mais compreensível, flexível e mais fácil de manter.

- **Single responsability principle:** Uma classe deve ter apenas uma única responsabilidade. Por ex: mudanças em apenas uma parte da especificação da aplicação deve ser capaz de afetar a especificação da classe.
- **Open/closed principle:** Entidades de software devem ser abertas para expansão, porém fechadas para modificação. Pense no uso de classes abstratas, que podem ser extendidas por classes concretas para novas funcionalidades, mas não alteradas diretamente para não impactar trechos de código que a utilizam.
- **Liskov substitution principle:** Objetos em um programa deve ser substituíveis com instâncias de seus subtipos sem alterar o funcionamento correto daquele programa. Considere leitura sobre [*design by contract*](https://en.wikipedia.org/wiki/Design_by_contract)*.* Se S é um subtipo de T, então objetos do tipo T podem ser substituídos por objetos do tipo S sem alterar qualquer propriedade de T.
- **Interface segregation principle:** Muitas interfaces específicas à um uso são melhores que apenas uma interface de uso geral.
- **Dependency inversion principle:** Uma funcionalidade deve depender de abstrações, não concretizações. Laravel utiliza fortemente este princípio.

## Notificações e E-mails

O Laravel possui Mailables e Notifications para envio de e-mails e notificações. Abaixo as
situações em que cada um deve ser utilizado:

**Mailables**
Os Mailables definem destinatários em cópia, assuntos, anexos e *view* utilizada para determinado
e-mail. Podem ser utilizadas em conjunto com as Notifications.

Não defina o destinatário principal nas Mailables. Eles devem ser definidos nos lugares em que
o e-mail é disparado, como por exemplo em Notifications e Listeners.

Evite ao máximo disparar e-mails diretamente de um Controller ou Job. Um e-mail normalmente
ocorre após um evento ocorrer, então dispare um evento, e através de um Listener envie o 
Mailable.

Envie Mailables diretamente, sem uso de Notifications, apenas quando o destinatário do e-mail
não é um usuário do site, acessível pelo *model* User.

**Notifications**
As notificações são a principal forma de enviar notificações, seja por e-mail, Slack, SMS ou
qalquer outra forma, para usuários do site. Sempre que você precise enviar uma notificação para
um usuário acessível por *model* User, utilize-as.