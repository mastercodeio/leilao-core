let mix = require('laravel-mix');

/*
 |--------------------------------------------------------------------------
 | Mix Asset Management
 |--------------------------------------------------------------------------
 |
 | Mix provides a clean, fluent API for defining some Webpack build steps
 | for your Laravel application. By default, we are compiling the Sass
 | file for the application as well as bundling up all the JS files.
 |
 */

mix.js('resources/assets/site/js/app.js', 'public/site/js')
    .sass('resources/assets/site/sass/app.scss', 'public/site/css')
    .copy('resources/assets/site/img', 'public/site/img');

mix.js('resources/assets/system/js/app.js', 'public/system/js')
    .sass('resources/assets/system/sass/app.scss', 'public/system/css')
    .copy('resources/assets/system/img', 'public/system/img');

mix.js('resources/assets/tenant/js/app.js', 'public/tenant/js')
    .sass('resources/assets/tenant/sass/app.scss', 'public/tenant/css')
    .copy('resources/assets/tenant/img', 'public/tenant/img');