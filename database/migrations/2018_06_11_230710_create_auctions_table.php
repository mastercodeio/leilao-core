<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateAuctionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('auctions', function (Blueprint $table) {
            common_migration_columns($table, true);
            tenant_foreign_key($table);
            $table->unsignedInteger('identifier');
            $table->string('title');
            $table->text('description');
            $table->string('address');
            $table->jsonb('location');

            $table->unique(['tenant_id', 'identifier']);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('auctions');
    }
}
