<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePhasesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('phases', function (Blueprint $table) {
            common_migration_columns($table, false);
            $table->timestamp('date');
            $table->unsignedSmallInteger('order')->default(0);
            $table->string('type', 20);
            $table->unsignedBigInteger('auction_id');

            $table->foreign('auction_id')->references('id')->on('auctions')->onCascade('delete');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('phases');
    }
}
