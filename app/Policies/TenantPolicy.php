<?php

namespace App\Policies;

use App\Models\System\Tenant;
use App\Models\System\Admin;
use App\Services\TenantService;
use Illuminate\Auth\Access\HandlesAuthorization;

class TenantPolicy
{
    use HandlesAuthorization;
    /**
     * @var TenantService
     */
    private $tenantService;

    /**
     * TenantPolicy constructor.
     *
     * @param TenantService $tenantService
     */
    public function __construct(TenantService $tenantService)
    {
        $this->tenantService = $tenantService;
    }

    /**
     * Determine whether the user can view the tenant.
     *
     * @param  \App\Models\System\Admin  $user
     * @param  \App\Models\System\Tenant $tenant
     * @return mixed
     */
    public function view(Admin $user, Tenant $tenant)
    {
        // TODO: Logic to check whether the user can view
        // the specified tenant site.
        return true;
    }

    /**
     * Determine whether the user can create tenants.
     *
     * @param  \App\Models\System\Admin $user
     * @return mixed
     */
    public function create(Admin $user)
    {
        //
    }

    /**
     * Determine whether the user can update the tenant.
     *
     * @param  \App\Models\System\Admin  $user
     * @param  \App\Models\System\Tenant $tenant
     * @return mixed
     */
    public function update(Admin $user, Tenant $tenant)
    {
        //
    }

    /**
     * Determine whether the user can delete the tenant.
     *
     * @param  \App\Models\System\Admin  $user
     * @param  \App\Models\System\Tenant $tenant
     * @return mixed
     */
    public function delete(Admin $user, Tenant $tenant)
    {
        //
    }
}
