<?php

namespace App\Listeners;

use App\Events\TenantConfirmed;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;

class NotifyTenantConfirmation
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  TenantConfirmed  $event
     * @return void
     */
    public function handle(TenantConfirmed $event)
    {
        //
    }
}
