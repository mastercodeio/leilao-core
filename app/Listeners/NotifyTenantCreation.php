<?php

namespace App\Listeners;

use App\Events\TenantCreated;
use App\Notifications\Tenant\ConfirmTenantRegistration;

class NotifyTenantCreation
{
    /**
     * Handle the event.
     *
     * @param  TenantCreated $event
     * @return void
     */
    public function handle(TenantCreated $event)
    {
        $event->user->notify(new ConfirmTenantRegistration($event->tenant));
    }
}
