<?php

namespace App\Events;

use App\Models\System\Tenant;
use App\Models\Tenant\User;
use Illuminate\Broadcasting\InteractsWithSockets;
use Illuminate\Broadcasting\PrivateChannel;
use Illuminate\Foundation\Events\Dispatchable;
use Illuminate\Queue\SerializesModels;

class TenantCreated
{
    use Dispatchable, InteractsWithSockets, SerializesModels;
    /**
     * @var Tenant
     */
    public $tenant;
    /**
     * @var User
     */
    public $user;

    /**
     * Create a new event instance.
     *
     * @param Tenant $tenant
     * @param User   $user
     */
    public function __construct(Tenant $tenant, User $user)
    {
        $this->tenant = $tenant;
        $this->user = $user;
    }

    /**
     * Get the channels the event should broadcast on.
     *
     * @return \Illuminate\Broadcasting\Channel|array
     */
    public function broadcastOn()
    {
        return new PrivateChannel('tenants');
    }
}
