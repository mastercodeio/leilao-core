<?php

namespace App\Providers;

use App\Services\AuctionService;
use App\Services\AuctionServiceContract;
use Blade;
use Illuminate\Support\Facades\View;
use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        Blade::component('tenant.components.breadcrumb', 'tenantBreadcrumb');

        View::composer('*', function ($view) {
            if (app()->has('tenant')) {
                $view->with('tenant', app()->get('tenant'));
            }
        });
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        // If the application is in local mode (development),
        // add the Service Provider for IDE support.
        if ($this->app->environment() === 'local') {
            $this->app->register(\Barryvdh\LaravelIdeHelper\IdeHelperServiceProvider::class);
        }

        // Register business service layers.
        $this->app->bind(AuctionServiceContract::class, AuctionService::class);
    }
}
