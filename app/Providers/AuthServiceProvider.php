<?php

namespace App\Providers;

use App\Models\System\Tenant;
use App\Policies\TenantPolicy;
use Auth;
use Illuminate\Auth\EloquentUserProvider;
use Illuminate\Foundation\Support\Providers\AuthServiceProvider as ServiceProvider;

class AuthServiceProvider extends ServiceProvider
{
    /**
     * The policy mappings for the application.
     *
     * @var array
     */
    protected $policies = [
        Tenant::class => TenantPolicy::class,
    ];

    /**
     * Register any authentication / authorization services.
     *
     * @return void
     */
    public function boot()
    {
        $this->registerPolicies();

        Auth::provider('tenant', function ($app, array $config) {
            // We are gonna try to connect to the authenticated tenant
            // and then add it as an instance to application.
            if ($app->session->has('tenant_id')) {
                $tenant = Tenant::findOrFail($app->session->get('tenant_id'));

                $app->instance('tenant', $tenant);
            }

            return new EloquentUserProvider($app->hash, $config['model']);
        });
    }
}
