<?php

namespace App\Providers;

use App\Menus\TenantMainMenu;
use Illuminate\Support\Facades\View;
use Illuminate\Support\ServiceProvider;

class MenuServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        View::composer('tenant.layouts.sidenavs.main', function ($view) {
            $view->with('sidenav_menu', new TenantMainMenu());
        });
    }

    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        //
    }
}
