<?php

namespace App\Http\Controllers\Site;

class HomeController extends SiteController
{
    public function index()
    {
        return view('site.home');
    }
}
