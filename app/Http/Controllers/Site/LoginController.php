<?php

namespace App\Http\Controllers\Site;

use App\Models\System\Tenant;
use App\Services\TenantService;
use Auth;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Http\Request;

class LoginController extends SiteController
{
    use AuthenticatesUsers;

    /** @var  Tenant */
    protected $tenant;
    /**
     * @var TenantService
     */
    private $tenantService;

    /**
     * LoginController constructor.
     *
     * @param TenantService $tenantService
     */
    public function __construct(TenantService $tenantService)
    {
        $this->middleware('guest:site')->except('logout');
        $this->tenantService = $tenantService;
    }

    /**
     * Redirect user to Account page after login.
     *
     * @return string
     */
    public function redirectTo()
    {
        return route('site.account.index');
    }

    /**
     * Get the guard to be used during authentication.
     *
     * @return \Illuminate\Contracts\Auth\StatefulGuard
     */
    protected function guard()
    {
        return Auth::guard('site');
    }

    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function showLoginForm()
    {
        return view('site.login');
    }

    /**
     * Attempt to login to the specified tenant.
     *
     * @param Request $request
     * @return bool
     */
    protected function attemptLogin(Request $request)
    {
        $attempt = $this->guard()->attempt(
            $this->credentials($request), $request->filled('remember')
        );

        if ($attempt === true) {
            return $this->guard()->user()->tenant->isConfirmed();
        }

        return false;
    }

    /**
     * Validate the login data.
     *
     * @param Request $request
     */
    protected function validateLogin(Request $request)
    {
        $this->validate($request, [
            'email'    => 'required|string',
            'password' => 'required|string',
        ]);
    }

    /**
     * Add a session data to identify the authenticated tenant.
     *
     * @param Request $request
     * @param         $user
     */
    protected function authenticated(Request $request, $user)
    {
        $request->session()->put('tenant_id', $user->tenant->getKey());
    }
}
