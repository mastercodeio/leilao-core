<?php

namespace App\Http\Controllers\Site;

use App\Http\Controllers\Site\SiteController as BaseController;
use App\Models\System\Tenant;
use App\Services\TenantService;
use Illuminate\Http\Request;
use Illuminate\Http\Response;

class AccountController extends BaseController
{
    /**
     * @var TenantService
     */
    private $tenantService;

    /**
     * AccountController constructor.
     *
     * @param TenantService $tenantService
     */
    public function __construct(TenantService $tenantService)
    {
        $this->tenantService = $tenantService;
    }

    /**
     * Confirm registration of the tenant.
     *
     * @param Request $request
     * @param Tenant  $tenant
     * @return string
     */
    public function confirm(Request $request, Tenant $tenant)
    {
        $acceptedStatuses = [
            Tenant::STATUS_PENDING_DOMAIN,
            Tenant::STATUS_PENDING_CONFIRMATION,
        ];

        abort_unless(
            $request->hasValidSignature() && in_array($tenant->status, $acceptedStatuses),
            Response::HTTP_NOT_FOUND,
            'URL inválida. Seu registro já pode ter sido confirmado e seu domínio configurado ;)'
        );

        // Confirm tenant registration if it's pending ...
        if ($tenant->hasStatus(Tenant::STATUS_PENDING_CONFIRMATION)) {
            $this->tenantService->confirmRegistration($tenant);
        }

        return redirect()
            ->route('site.login.form')
            ->with('success', "O cadastro de {$tenant->name} foi confirmado!");
    }

    /**
     * Show the index page of the tenant.
     *
     */
    public function index()
    {
        return view('site.account.index')->with('tenant', app('tenant'));
    }

    /**
     * Update the tenant domain.
     *
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function updateDomain(Request $request)
    {
        // TODO: Move this code to a Form Request, and add a
        // Validation Rule to check a valid domain.
        $input = $request->validate([
            'domain' => 'required|max:200',
        ]);

        $this->tenantService->updateDomain(app('tenant'), $input['domain']);

        return redirect()->route('site.account.index')->with('success', 'O cadastro do domínio foi realizado com sucesso.');
    }
}
