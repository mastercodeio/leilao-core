<?php

namespace App\Http\Controllers\Site;

use App\Http\Requests\Site\StoreRegisterBaseDataRequest;
use App\Services\TenantService;

class RegisterController extends SiteController
{
    /**
     * @var TenantService
     */
    private $tenantService;

    /**
     * RegisterController constructor.
     *
     * @param TenantService $tenantService
     */
    public function __construct(TenantService $tenantService)
    {
        $this->tenantService = $tenantService;
    }

    /**
     * Show the base form, which is used to
     * register information about the tenant
     * and its owner.
     */
    public function showBaseForm()
    {
        return view('site.register.form');
    }

    /**
     * Save the register information required
     * in the base form.
     *
     * @param StoreRegisterBaseDataRequest $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function storeBaseData(StoreRegisterBaseDataRequest $request)
    {
        $data = $this->tenantService->create($request->validated());

        return redirect()
            ->route('site.register.welcome')
            ->with('tenant', $data->get('tenant')->getKey())
            ->with('username', $data->get('user')->name);
    }

    /**
     * Hey, we just got a new client, so let's say
     * Hi to them (and ask them to confirm their
     * email too). :D
     */
    public function welcome()
    {
        if (!session('username') || !session('tenant')) {
            return redirect()->route('site.home');
        }

        return view('site.register.welcome')->with([
            'user_name' => array_first(explode(' ', session('username'))),
        ]);
    }
}