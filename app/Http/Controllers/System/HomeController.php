<?php

namespace App\Http\Controllers\System;

class HomeController extends Controller
{
    public function index()
    {
        return view('system.home');
    }
}
