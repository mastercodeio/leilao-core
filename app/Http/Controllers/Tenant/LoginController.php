<?php

namespace App\Http\Controllers\Tenant;

use App\Models\System\Tenant;
use App\Services\TenantService;
use Auth;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Http\Request;

class LoginController extends TenantController
{
    use AuthenticatesUsers;

    /** @var  Tenant */
    protected $tenant;
    /**
     * @var TenantService
     */
    private $tenantService;

    /**
     * LoginController constructor.
     *
     * @param TenantService $tenantService
     */
    public function __construct(TenantService $tenantService)
    {
        $this->middleware('guest:tenant')->except('logout');
        $this->tenantService = $tenantService;
    }

    /**
     * Redirect user to Account page after login.
     *
     * @return string
     */
    public function redirectTo()
    {
        return route('tenant.main.dashboard');
    }

    /**
     * Get the guard to be used during authentication.
     *
     * @return \Illuminate\Contracts\Auth\StatefulGuard
     */
    protected function guard()
    {
        return Auth::guard('tenant');
    }

    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function showLoginForm()
    {
        return view('tenant.login');
    }

    /**
     * Attempt to login to the specified tenant.
     *
     * @param Request $request
     * @return bool
     */
    protected function attemptLogin(Request $request)
    {
        return $this->guard()->attempt(
            $this->credentials($request), $request->filled('remember')
        );
    }

    /**
     * Validate the login data.
     *
     * @param Request $request
     */
    protected function validateLogin(Request $request)
    {
        $this->validate($request, [
            'email'    => 'required|string',
            'password' => 'required|string',
        ]);
    }
}
