<?php

namespace App\Http\Controllers\Tenant;

use Illuminate\Support\Facades\Storage;

class FileController extends TenantController
{
    public function stream(string $file)
    {
        $fullPath = app('tenant')->getKey() . '/' . $file;

        abort_unless(Storage::disk('tenant')->exists($file), 404);

        return response()->download(storage_path('app/tenant/' . $fullPath), null, [], null);
    }
}
