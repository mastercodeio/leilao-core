<?php

namespace App\Http\Controllers\Tenant\Main;

use App\Http\Controllers\Tenant\TenantController;

class DashboardController extends TenantController
{
    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('tenant.main.dashboard');
    }
}
