<?php

namespace App\Http\Controllers\Tenant\Main;

use App\Http\Controllers\Tenant\TenantController;
use App\Models\Tenant\Auction;
use App\Services\AuctionServiceContract;
use Illuminate\Http\Request;

class AuctionController extends TenantController
{
    /**
     * @var AuctionServiceContract
     */
    private $auctionService;

    /**
     * AuctionController constructor.
     *
     * @param AuctionServiceContract $auctionService
     */
    public function __construct(AuctionServiceContract $auctionService)
    {
        $this->auctionService = $auctionService;
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $items = Auction::all();

        return view('tenant.main.auctions.index')
            ->with('items', $items)
            ->with('table_title', 'Lista de leilões ativos');
    }

    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function create()
    {
        $identifier = Auction::getNewIdentifier();
        $item = new Auction(compact('identifier'));

        return view('tenant.main.auctions.form')
            ->with('item', $item)
            ->with('page_title', 'Adicionar Leilão');
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function store(Request $request)
    {
        $item = $this->auctionService->create(app('tenant'), $request->all());

        return redirect()
            ->route('tenant.main.auctions.edit', $item)
            ->with('success', "Leilão {$item->identifier} adicionado com sucesso.");
    }

    /**
     * @param Auction $item
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function edit(Auction $item)
    {
        return view('tenant.main.auctions.form')
            ->with('item', $item)
            ->with('page_title', 'Editar Leilão ' . $item->identifier_label);
    }

    /**
     * @param Request $request
     * @param Auction $item
     * @return \Illuminate\Http\RedirectResponse
     */
    public function update(Request $request, Auction $item)
    {
        $this->auctionService->update($item, $request->all());

        return redirect()
            ->route('tenant.main.auctions.edit', $item)
            ->with('success', "Leilão {$item->identifier} atualizado com sucesso.");
    }

    /**
     *
     */
    public function destroy()
    {
        //
    }
}
