<?php

namespace App\Http\Controllers\Tenant\Main;

use App\Http\Controllers\Controller;
use App\Models\Tenant\Category;
use Illuminate\Http\Request;

class CategoryController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('tenant.main.categories.index')->with('items', Category::all());
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('tenant.main.categories.form')
            ->with('title', 'Adicionar Categoria de Lote')
            ->with('item', new Category());
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $data = $request->validate([
            'title' => 'required|unique:categories,title',
        ]);

        $item = Category::create($data);

        return redirect()->route('tenant.main.categories.edit', $item);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Category $category)
    {
        return view('tenant.main.categories.form')
            ->with('title', 'Atualizar Categoria de Lote')
            ->with('item', $category);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param Category                  $category
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Category $category)
    {
        $data = $request->validate([
            'title' => 'required|unique:categories,title,' . $category->getKey(),
        ]);

        $category->fill($data)->save();

        return redirect()
            ->route('tenant.main.categories.edit', $category)
            ->with('success', 'Categoria de Lote atualizada com sucesso.');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
