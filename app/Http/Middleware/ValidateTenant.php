<?php

namespace App\Http\Middleware;

use App\Models\System\Tenant;
use Closure;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Config;

class ValidateTenant
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  \Closure                 $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if ($this->hasTenant($request)) {
            Config::set('app.url', config('app.protocol') . '://' . app('tenant')->domain);
            Config::set('filesystems.disks.tenant.root', storage_path('app/tenant/' . app('tenant')->getKey()));
            Config::set('filesystems.disks.tenant.url', config('app.url') . '/media');
            return $next($request);
        }

        abort(404);
    }

    protected function hasTenant($request)
    {
        return (app()->has('tenant')
            || $this->validateByDomain($request)
            || $this->validateByParameter($request));
    }

    /**
     * Search and validate the tenant by the domain being visited.
     *
     * @param Request $request
     * @return bool
     */
    protected function validateByDomain(Request $request): bool
    {
        $domain = parse_url($request->url(), PHP_URL_HOST);

        // Remove www prefix if necessary and check whether
        // the domain is the same of the website.
        // When domain is the same as of the website, we
        // return false so we can verify the tenant using
        // other method.
        if (strpos($domain, 'www.') === 0) {
            $domain = substr($domain, 4);
        }

        if ($domain === config('app.domain')) {
            return false;
        }

        // Look for a tenant by domain
        if ($tenant = Tenant::domain($domain)->first()) {
            app()->instance('tenant', $tenant);
            return true;
        }

        return false;
    }

    /**
     * Search and validate the tenant by the parameter provided
     * on the URL.
     *
     * @param Request $request
     * @return bool
     */
    protected function validateByParameter(Request $request): bool
    {
        if ($tenant = $request->route('tenant')) {
            app()->instance('tenant', $tenant);
            return true;
        }

        return false;
    }
}
