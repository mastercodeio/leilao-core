<?php

namespace App\Http\Requests\Site;

use Illuminate\Foundation\Http\FormRequest;

class StoreRegisterBaseDataRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'tenant'                   => 'required|array',
            // TODO: Add a custom rule to validate the registration code
            'tenant.registration_code' => 'required|max:10|unique:system.tenants,registration_code',
            'tenant.name'              => 'required|string|max:200',
            'name'                     => 'required|string|max:200',
            'email'                    => 'required|string|max:200',
            'password'                 => 'required|min:8|max:200|confirmed',
        ];
    }
}
