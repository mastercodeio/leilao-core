<?php

/**
 * HELPERS
 */

if (!function_exists('tenant_route')) {
    /**
     * Get a url to a tenant route.
     *
     * @param string $route
     * @param array  $data
     * @return string
     */
    function tenant_route(string $route, array $data = [])
    {
        return app()->tenant->domain . route($route, $data, false);
    }
}

if (!function_exists('common_migration_columns')) {
    /**
     * Add common columns to a migration.
     *
     * @param \Illuminate\Database\Schema\Blueprint $table
     * @param bool                                  $hashid
     */
    function common_migration_columns(\Illuminate\Database\Schema\Blueprint $table, $hashid)
    {
        $table->bigIncrements('id');

        if ($hashid === true) {
            $table->string('hashid', 16)->nullable()->unique();
        }

        $table->timestamps();
        $table->softDeletes();
    }
}

if (!function_exists('tenant_foreign_key')) {
    /**
     * Add a foreign key relationship with Tenant to a migration.
     *
     * @param \Illuminate\Database\Schema\Blueprint $table
     */
    function tenant_foreign_key(\Illuminate\Database\Schema\Blueprint $table)
    {
        $table->unsignedBigInteger('tenant_id');
        $table->foreign('tenant_id')->references('id')->on('tenants')->onCascade('delete');
    }
}