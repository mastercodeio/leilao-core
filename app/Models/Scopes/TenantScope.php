<?php
/**
 * Created by PhpStorm.
 * User: harry
 * Date: 06/07/2018
 * Time: 11:30
 */

namespace App\Models;

use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Scope;

class TenantScope implements Scope
{
    /**
     * Apply the scope to a given Eloquent query builder.
     *
     * @param  \Illuminate\Database\Eloquent\Builder $builder
     * @param  \Illuminate\Database\Eloquent\Model   $model
     * @return void
     */
    public function apply(Builder $builder, Model $model)
    {
        if (!is_null(app('tenant'))) {
            $builder->where('tenant_id', '=', app('tenant')->getKey());
        }
    }
}