<?php

namespace App\Models\Tenant;

use App\Models\Tenant\Embeds\Type;
use App\Models\TenantModel;
use App\Models\Traits\BelongsToAuction;
use Illuminate\Support\Collection;

class Phase extends TenantModel
{
    use BelongsToAuction;

    protected $fillable = [
        'date',
        'types',
    ];

    protected $casts = [
        'date'       => 'datetime',
        'auction_id' => 'integer',
    ];

    /**
     * @return Collection
     */
    public function getTypesAttribute(): Collection
    {
        if (is_null($this->attributes['types'])) {
            return collect([]);
        }

        return collect(self::fromJson($this->attributes['types']))
            ->map(function ($item) {
                return new Type(['slug' => $item]);
            });
    }

    /**
     * @param Collection $value
     * @return string
     */
    public function setTypesAttribute(Collection $value)
    {
        $this->attributes['types'] = $value->map(function ($item) {
            return (string)$item;
        })->toJson();
    }
}
