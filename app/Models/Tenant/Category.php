<?php

namespace App\Models\Tenant;

use App\Models\TenantModel;
use App\Models\Traits\BelongsToTenant;
use App\Models\Traits\Sluggable;

class Category extends TenantModel
{
    use Sluggable, BelongsToTenant;

    protected $fillable = [
        'title',
        'slug',
    ];

    function sluggable(): string
    {
        return $this->title;
    }

    public function url($action): string
    {
        if ($action === 'form') {
            $action = $this->getKey() ? 'update' : 'store';
        }
        
        if ($this->getKey()) {
            return route("tenant.main.categories.{$action}", $this->getKey());
        }

        return route("tenant.main.categories.{$action}");
    }
}
