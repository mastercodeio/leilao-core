<?php

namespace App\Models\Tenant;

use App\Models\System\Tenant;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;

/**
 * Class User
 *
 * @property $name
 * @property $email
 * @property $password
 * @property $role
 * @package App\Models\Tenant
 */
class User extends Authenticatable
{
    use Notifiable, SoftDeletes;

    const STATUS_ACTIVE = 'active';
    const STATUS_INACTIVE = 'inactive';
    const STATUS_BLOCKED = 'blocked';

    const ROLE_SUPERADMIN = 'superadmin';
    const ROLE_ADMIN = 'admin';
    const ROLE_USER = 'user';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name',
        'email',
        'role',
        'password',
    ];

    /**
     * @var array
     */
    protected $casts = [];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password',
        'remember_token',
    ];

    /**
     * The tenant which the user is registered on.
     *
     * @return BelongsTo
     */
    public function tenant(): BelongsTo
    {
        return $this->belongsTo(Tenant::class);
    }

    /**
     * Encrypt the password string.
     *
     * @param string $value
     */
    public function setPasswordAttribute(string $value)
    {
        $this->attributes['password'] = bcrypt($value);
    }

    /**
     * Get the user's first name.
     *
     * @return mixed
     */
    public function getFirstNameAttribute()
    {
        return explode(' ', $this->getAttribute('name'))[0];
    }

    public static function createSuperAdmin(Tenant $tenant, array $data): User
    {
        $user = $tenant->users()->create(
            $data + ['role' => self::ROLE_SUPERADMIN]
        );

        // TODO: event Tenant\SuperAdminCreated

        return $user;
    }
}
