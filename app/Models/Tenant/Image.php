<?php

namespace App\Models\Tenant;

use App\Models\TenantModel;
use App\Models\Traits\Hashable;
use Illuminate\Database\Eloquent\Relations\MorphTo;
use Illuminate\Http\UploadedFile;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Str;

/**
 * @property  uploaded_file
 * @property UploadedFile|null uploaded_file
 */
class Image extends TenantModel
{
    use Hashable;

    /** @var UploadedFile */
    public $uploadedFile;

    protected $fillable = [
        'file',
        'path',
        'name',
        'mimetype',
        'tag',
    ];

    protected static function boot()
    {
        static::saving(function (Image $image) {
            if (isset($image->uploadedFile)) {
                $image->mimetype = $image->uploadedFile->getMimeType();
                $image->file = ($image->file ?? Str::random(40) . strtotime(time())) . '.' . $image->uploadedFile->guessExtension();
                $image->name = $image->name ?? $image->uploadedFile->getBasename();
                $image->uploadedFile->storeAs($image->path, $image->file, 'tenant');
                $image->uploadedFile = null;
            }
        });

        static::deleted(function (Image $image) {
            $image->removeFile();
        });
    }

    /**
     * @return MorphTo
     */
    public function imageable(): MorphTo
    {
        return $this->morphTo();
    }

    /**
     * Set the uploaded file to be used.
     *
     * @param UploadedFile $file
     */
    public function setUploadedFileAttribute(UploadedFile $file)
    {
        $this->uploadedFile = $file;
    }

    /**
     * Return the full path of the file.
     *
     * @return string
     */
    public function getFullPathAttribute(): string
    {
        return "{$this->path}/{$this->file}";
    }

    /**
     * Get the instance of the Disk Storage related to tenant files.
     *
     * @return \Illuminate\Contracts\Filesystem\Filesystem|\Illuminate\Filesystem\FilesystemAdapter
     */
    public function getDiskAttribute()
    {
        return Storage::disk('tenant');
    }

    /**
     * Save the actual file to the filesystem.
     *
     * @param UploadedFile $file
     * @param              $path
     * @param null         $name
     * @return string
     */
    protected function saveFile(UploadedFile $file, $path, $name = null): string
    {
        $filename = ($name ?? Str::random(40 . strtotime(time()))) . $file->guessExtension();
        $file->storeAs($path, $filename, 'tenant');

        return $filename;
    }

    /**
     * @return string
     */
    protected function getUrlAttribute(): string
    {
        return $this->disk->url($this->full_path);
    }

    /**
     * Remove the actual file.
     */
    protected function removeFile()
    {
        $this->disk->delete($this->full_path);
    }
}
