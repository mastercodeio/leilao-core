<?php

namespace App\Models\Tenant\Embeds;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Collection;

class Type extends Model
{
    const TYPE_ONLINE = 'online';
    const TYPE_LIVE = 'live';

    const TYPES = [
        self::TYPE_ONLINE => 'On-line',
        self::TYPE_LIVE   => 'Presencial',
    ];

    protected $fillable = [
        'slug',
    ];

    /**
     * Ensure that only valid types are saved on the database.
     *
     * @param string $value
     * @throws \Exception
     */
    public function setSlugAttribute(string $value)
    {
        if (!array_has(self::TYPES, $value)) {
            throw new \Exception('Invalid auction type');
        }

        $this->attributes['slug'] = $value;
        $this->attributes['label'] = static::TYPES[$value];
    }

    /**
     * @return string
     */
    public function getLabelAttribute(): string
    {
        return self::TYPES[$this->getAttribute('slug')];
    }

    /**
     * @return string
     * @throws \Throwable
     */
    public function render(): string
    {
        if ($this->slug === self::TYPE_LIVE) {
            return view('tenant.components.auction-type', [
                'icon'  => 'fas fa-home',
                'class' => 'tag is-warning',
                'slot'  => '',
            ])->render();
        }

        if ($this->slug === self::TYPE_ONLINE) {
            return view('tenant.components.auction-type', [
                'icon'  => 'fas fa-desktop',
                'class' => 'tag is-primary',
                'slot'  => '',
            ])->render();
        }

        return '';
    }

    static public function getSelectOptions(): Collection
    {
        return collect(static::TYPES)->map(function ($label, $value) {
            return compact('label', 'value');
        })->values();
    }

    /**
     * @return mixed|string
     */
    public function __toString()
    {
        return $this->slug;
    }
}
