<?php

namespace App\Models\Tenant;

use App\Models\TenantModel;
use App\Models\Traits\BelongsToTenant;
use App\Models\Traits\Hashable;
use App\Models\Types\Location;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Database\Eloquent\Relations\HasOne;
use Illuminate\Database\Eloquent\Relations\MorphOne;

/**
 * @property mixed phases
 * @property mixed logo
 * @property mixed banner
 */
class Auction extends TenantModel
{
    use Hashable, BelongsToTenant;

    /** @var array */
    protected $fillable = [
        'title',
        'identifier',
        'description',
        'location',
        'address',
    ];

    /** @var array */
    protected $casts = [
        'identifier' => 'integer',
    ];

    /** @var array */
    protected $with = ['phases'];

    /**
     * @return HasOne
     */
    public function logo(): MorphOne
    {
        return $this->morphOne(Image::class, 'imageable')->where('tag', '=', 'logo')->latest();
    }

    /**
     * @return HasOne
     */
    public function banner(): MorphOne
    {
        return $this->morphOne(Image::class, 'imageable')->where('tag', '=', 'banner')->latest();
    }

    /**
     * An auction may have many phases. Each phase defines a date which
     * the auction will run.
     *
     * @return HasMany
     */
    public function phases(): HasMany
    {
        return $this->hasMany(Phase::class);
    }

    /**
     * @param $action
     * @return string
     */
    public function url($action): string
    {
        if ($action === 'save') {
            if ($this->exists) {
                return route("tenant.main.auctions.update", $this);
            }

            return route("tenant.main.auctions.store");
        }

        return route("tenant.main.auctions.{$action}", $this);
    }

    /**
     * Get a "prettier" representation of the Identifier integer.
     *
     * @return string
     */
    public function getIdentifierLabelAttribute(): string
    {
        $a = substr($this->identifier, 0, 4);
        $b = substr($this->identifier, 4, 3);

        return "$a/$b";
    }

    /**
     * Get a SpatialPoint class based on the actual location attribute.
     *
     * @return Location|null
     */
    public function getLocationAttribute(): ?Location
    {
        $l = !empty($this->attributes['location']) ? self::fromJson($this->attributes['location']) : null;
        $spatialPoint = new Location();

        if (!is_null($spatialPoint)) {
            $spatialPoint->setLatitude($l['latitude'])->setLongitude($l['longitude']);
        }

        return $spatialPoint;
    }

    /**
     * Set the actual location attribute to be a format accepted by MongoDB.
     *
     * @param Location $point
     */
    public function setLocationAttribute(Location $point)
    {
        $this->attributes['location'] = $point->toJson();
    }

    /**
     * @return string
     */
    public static function urlCreate()
    {
        return route('tenant.main.auctions.create');
    }

    /**
     * Get a new identifier, based on the current year and maximum identifier found
     * in the database.
     *
     * @return int
     */
    public static function getNewIdentifier(): int
    {
        $year = date('Y');

        $max = ((new Auction)
                    ->where('identifier', '>', $year . '000')
                    ->max('identifier') ?? intval($year . '000')) + 1;

        return (int)str_pad($max, 3, 0, STR_PAD_LEFT);
    }
}
