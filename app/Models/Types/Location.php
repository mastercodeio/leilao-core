<?php
/**
 * Created by PhpStorm.
 * User: harry
 * Date: 11/06/18
 * Time: 20:16
 */

namespace App\Models\Types;

class Location
{
    /** @var float */
    protected $latitude;

    /** @var float */
    protected $longitude;

    /**
     * @return float
     */
    public function getLatitude()
    {
        return $this->latitude;
    }

    /**
     * Set the latitude.
     *
     * @param $value
     * @return Location
     */
    public function setLatitude($value)
    {
        $this->latitude = $value;
        return $this;
    }

    /**
     * @return float
     */
    public function getLongitude()
    {
        return $this->longitude;
    }

    /**
     * Set the longitude.
     *
     * @param $value
     * @return Location
     */
    public function setLongitude($value)
    {
        $this->longitude = $value;
        return $this;
    }

    /**
     * Return an array from coordinates.
     *
     * @return array
     */
    public function toArray(): array
    {
        return [
            'latitude'  => $this->latitude,
            'longitude' => $this->longitude,
        ];
    }

    /**
     * Return a JSON string.
     *
     * @return string
     */
    public function toJson(): string
    {
        return collect($this->toArray())->toJson();
    }

    /**
     * Return a string version of the coordinates.
     *
     * @return string
     */
    public function __toString()
    {
        if (!$this->latitude || !$this->longitude) {
            return '';
        }

        return "{$this->latitude} - {$this->longitude}";
    }
}