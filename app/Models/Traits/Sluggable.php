<?php
/**
 * Created by PhpStorm.
 * User: harry
 * Date: 01/06/18
 * Time: 00:50
 */

namespace App\Models\Traits;

use App\Models\Observers\SluggableObserver;

trait Sluggable
{
    protected static function bootSluggable()
    {
        static::observe(SluggableObserver::class);
    }

    abstract function sluggable(): string;
}