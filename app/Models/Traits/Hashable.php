<?php
/**
 * Created by PhpStorm.
 * User: harry
 * Date: 01/06/18
 * Time: 00:50
 */

namespace App\Models\Traits;

use App\Models\Observers\HashableObserver;

trait Hashable
{
    protected static function bootHashable()
    {
        static::observe(HashableObserver::class);
    }

    public function getRouteKeyName()
    {
        return 'hashid';
    }
}