<?php
/**
 * Created by PhpStorm.
 * User: harry
 * Date: 07/07/2018
 * Time: 14:18
 */

namespace App\Models\Traits;

use App\Models\Tenant\Auction;
use Illuminate\Database\Eloquent\Relations\BelongsTo;

trait BelongsToAuction
{
    /**
     * @return BelongsTo
     */
    public function auction(): BelongsTo
    {
        return $this->belongsTo(Auction::class);
    }
}