<?php
/**
 * Created by PhpStorm.
 * User: harry
 * Date: 07/07/2018
 * Time: 14:18
 */

namespace App\Models\Traits;

use App\Models\System\Tenant;
use App\Models\TenantScope;
use Illuminate\Database\Eloquent\Relations\BelongsTo;

trait BelongsToTenant
{
    /**
     * Tell Laravel to boot the trait.
     */
    protected static function bootBelongsToTenant()
    {
        static::addGlobalScope(new TenantScope);
    }

    /**
     * @return BelongsTo
     */
    public function tenant(): BelongsTo
    {
        return $this->belongsTo(Tenant::class);
    }
}