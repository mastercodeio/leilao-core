<?php

namespace App\Models\System;

use App\Models\System\Observers\TenantObserver;
use App\Models\SystemModel;
use App\Models\Tenant\Auction;
use App\Models\Tenant\User;
use Illuminate\Database\Eloquent\Relations\HasMany;

/**
 * Class Tenant
 *
 * @package App\Models\System
 */
class Tenant extends SystemModel
{
    protected $fillable = [
        'name',
        'registration_code',
        'domain',
        'status',
    ];

    const STATUS_ACTIVE = 'active';
    const STATUS_INACTIVE = 'inactive';
    const STATUS_BLOCKED = 'blocked';
    const STATUS_PENDING_PAYMENT = 'pending-payment';
    const STATUS_PENDING_DOMAIN = 'pending-domain';
    const STATUS_PENDING_CONFIRMATION = 'pending-confirmation';

    protected static function boot()
    {
        parent::boot();
        static::observe(TenantObserver::class);
    }

    /**
     * Users registered in the tenant.
     *
     * @return HasMany
     */
    public function users(): HasMany
    {
        return $this->hasMany(User::class);
    }

    /**
     * List of auctions of the tenant.
     *
     * @return HasMany
     */
    public function auctions(): HasMany
    {
        return $this->hasMany(Auction::class);
    }

    /**
     * Filter by tenant domain.
     *
     * @param        $query
     * @param string $domain
     */
    public function scopeDomain($query, string $domain)
    {
        $query->where('domain', $domain);
    }

    /**
     * Filter by list of tenant domains.
     *
     * @param       $query
     * @param array $domains
     */
    public function scopeDomains($query, array $domains)
    {
        $query->whereIn('domain', $domains);
    }

    /**
     * Filter tenants by its names.
     *
     * @param        $query
     * @param string $name
     */
    public function scopeName($query, string $name)
    {
        $query->where('name', $name);
    }

    /**
     * Filter tenants by its registration codes.
     *
     * @param        $query
     * @param string $code
     */
    public function scopeRegistrationCode($query, string $code)
    {
        $query->where('registration_code', $code);
    }

    /**
     * Include only tenants which are confirmed.
     *
     * @param $query
     */
    public function scopeConfirmed($query)
    {
        $query->where('status', '!=', self::STATUS_PENDING_CONFIRMATION);
    }

    /**
     * Determine whether the tenant was confirmed yet.
     *
     * @return bool
     */
    public function isConfirmed(): bool
    {
        return $this->attributes['status'] !== self::STATUS_PENDING_CONFIRMATION;
    }

    /**
     * Determine whether the tenant has the specified status.
     *
     * @param string $status
     * @return bool
     */
    public function hasStatus(string $status): bool
    {
        return $this->getAttribute('status') === $status;
    }

    /**
     * Determine whether the tenant has a domain set up.
     *
     * @return bool
     */
    public function hasDomainSetup(): bool
    {
        return !$this->hasStatus(self::STATUS_PENDING_DOMAIN) && !is_null($this->getAttribute('domain'));
    }

    public function route($route, $data = []): string
    {
        return '//' . $this->domain . route($route, $data, false);
    }
}
