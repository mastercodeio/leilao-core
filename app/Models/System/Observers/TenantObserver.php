<?php
/**
 * Created by PhpStorm.
 * User: harry
 * Date: 4/10/18
 * Time: 3:44 AM
 */

namespace App\Models\System\Observers;

use App\Models\System\Tenant;

class TenantObserver
{
    public function creating($tenant)
    {
        $tenant->fill([
            'status' => Tenant::STATUS_PENDING_CONFIRMATION,
        ]);
    }
}