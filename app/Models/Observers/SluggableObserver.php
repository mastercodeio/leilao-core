<?php
/**
 * Created by PhpStorm.
 * User: harry
 * Date: 01/06/18
 * Time: 00:41
 */

namespace App\Models\Observers;

use Illuminate\Database\Eloquent\Model;

class SluggableObserver
{
    public function creating(Model $model)
    {
        $this->handleSlug($model);
    }

    public function updating(Model $model)
    {
        $this->handleSlug($model);
    }

    public function handleSlug(Model $model)
    {
        $slug = str_slug($model->sluggable());

        if ($model->getKey() && $model->slug === $slug) {
            return;
        }

        $slugCount = $model->newQuery()->where('slug', $slug)->count() + 1;

        $model->slug = $slugCount > 1 ? "{$slug}-{$slugCount}" : $slug;
    }
}