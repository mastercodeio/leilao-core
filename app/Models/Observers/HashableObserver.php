<?php
/**
 * Created by PhpStorm.
 * User: harry
 * Date: 01/06/18
 * Time: 00:41
 */

namespace App\Models\Observers;

use Hashids\Hashids;
use Illuminate\Database\Eloquent\Model;

class HashableObserver
{
    public function created(Model $model)
    {
        $hashids = new Hashids(get_class($model), 8);

        $model->hashid = $hashids->encode($model->getKey());
        $model->save();
    }
}