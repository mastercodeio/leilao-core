<?php

namespace App\Rules;

use Illuminate\Contracts\Validation\Rule;

class CnpjRule implements Rule
{
    /**
     * Create a new rule instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Determine if the validation rule passes.
     *
     * @param  string $attribute
     * @param  mixed  $value
     * @return bool
     */
    public function passes($attribute, $value)
    {
        // TODO: Add validation of CNPJ digits, to check
        // if the Verification Numbers (last two) are
        // correct.

        return preg_match('/^\d{14}/', $value);
    }

    /**
     * Get the validation error message.
     *
     * @return string
     */
    public function message()
    {
        return 'O CNPJ informado é inválido.';
    }
}
