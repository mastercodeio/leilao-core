<?php
/**
 * Created by PhpStorm.
 * User: harry
 * Date: 13/07/2018
 * Time: 02:58
 */

namespace App\Services;

use App\Models\System\Tenant;
use App\Models\Tenant\Auction;
use App\Models\Tenant\Image;
use App\Models\Types\Location;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\UploadedFile;
use Validator;

class AuctionService implements AuctionServiceContract
{
    /**
     * Define the rules to be used on validations.
     *
     * @return array
     */
    public function rulesCreate(): array
    {
        return [
            'identifier'         => 'required|unique:auctions,identifier|regex:/^\d{7}$/',
            'title'              => 'required|min:1|max:180',
            'description'        => 'required|max:5000',
            'location'           => 'required|array',
            'location.address'   => 'required',
            'location.longitude' => 'required', // TODO: Add rule to validate geolocation
            'location.latitude'  => 'required', // TODO: Add rule to validate geolocation
            'logo'               => 'nullable|image',
            'banner'             => 'nullable|image',
        ];
    }

    /**
     * Define the validation rules to be used on update.
     *
     * @return array
     */
    public function rulesUpdate(): array
    {
        return [
            'title'              => 'required|min:1|max:180',
            'description'        => 'required|max:5000',
            'location'           => 'required|array', // TODO: Add rule to validate geolocation
            'location.address'   => 'required', // TODO: Add rule to validate geolocation
            'location.longitude' => 'required', // TODO: Add rule to validate geolocation
            'location.latitude'  => 'required', // TODO: Add rule to validate geolocation
            'logo'               => 'nullable|image',
            'logo_remove'        => 'nullable|boolean',
            'banner'             => 'nullable|image',
            'banner_remove'      => 'nullable|boolean',
            'phases'             => 'nullable|array',
            'phases.*.id'        => 'nullable|integer|exists:phases,id',
            'phases.*.date'      => 'required|date_format:d/m/Y',
            'phases.*.types'     => 'required|array|min:1',
        ];
    }

    /**
     * @param array  $data
     * @param Tenant $tenant
     * @return \Illuminate\Database\Eloquent\Model
     * @throws \Illuminate\Validation\ValidationException
     */
    public function create(Tenant $tenant, array $data): Model
    {
        $data = Validator::validate($data, $this->rulesCreate());

        // Transform the address and location to the accepted format
        $item = $tenant->auctions()->create(
            array_merge($data, $this->prepareLocation($data))
        );

        $this->updateAuctionLogo($item, $data['logo'] ?? null);
        $this->updateAuctionBanner($item, $data['banner'] ?? null);

        return $item;
    }

    /**
     * @param Auction $item
     * @param array   $data
     * @return Auction
     * @throws \Illuminate\Validation\ValidationException
     */
    public function update(Auction $item, array $data): Auction
    {
        $data = Validator::validate($data, $this->rulesUpdate());

        $this->updateAuctionInfo($item, $data)
            ->updateAuctionPhases($item, $data['phases'])
            ->updateAuctionLogo($item, $data['logo'] ?? null, $data['logo_remove'])
            ->updateAuctionBanner($item, $data['banner'] ?? null, $data['banner_remove']);

        return $item;
    }

    /**
     * Update information of the specified auction.
     *
     * @param Auction $item
     * @param array   $data
     * @return $this
     */
    public function updateAuctionInfo(Auction $item, array $data)
    {
        $data = array_merge($data, $this->prepareLocation($data));

        $item->fill($data);
        $item->save();

        return $this;
    }

    /**
     * Update phases of the specified auction.
     *
     * @param Auction $item
     * @param array   $data
     * @return $this
     */
    public function updateAuctionPhases(Auction $item, array $data)
    {
        $item->load('phases');

        // First we remove phases that are not found on $data array.
        $item->phases->whereNotIn('id', array_pluck($data, 'id'))->each->delete();

        // Then we create or update phases according to $data array.
        if (!empty($data)) {
            collect($data)->each(function ($phase) use ($item) {
                $d = [
                    'date'  => Carbon::createFromFormat('d/m/Y', $phase['date']),
                    'types' => collect($phase['types']),
                ];

                if (isset($phase['id'])) {
                    $item->phases->where('id', $phase['id'])->first()->update($d);
                    return;
                }

                $item->phases()->create($d);
            });
        }

        return $this;
    }

    /**
     * Update an auction logo.
     *
     * @param Auction      $auction
     * @param UploadedFile $file
     * @param bool         $removePrevious
     * @return $this
     */
    public function updateAuctionLogo(Auction $auction, ?UploadedFile $file, bool $removePrevious = false)
    {
        if ($removePrevious && !is_null($auction->logo)) {
            $auction->logo->forceDelete();
        }

        if (!is_null($file)) {
            if (!is_null($auction->logo)) {
                $auction->logo->forceDelete();
            }

            $image = new Image([
                'path' => 'auctions-' . $auction->getKey(),
                'tag'  => 'logo',
            ]);

            $image->uploaded_file = $file;
            $auction->logo()->save($image);
        }

        return $this;
    }

    /**
     * Update an auction banner.
     *
     * @param Auction      $auction
     * @param UploadedFile $file
     * @param bool         $removePrevious
     * @return $this
     */
    public function updateAuctionBanner(Auction $auction, ?UploadedFile $file, bool $removePrevious = false)
    {
        if ($removePrevious && !is_null($auction->banner)) {
            $auction->banner->forceDelete();
        }

        if (!is_null($file)) {
            if (!is_null($auction->banner)) {
                $auction->banner->forceDelete();
            }

            $image = new Image([
                'path' => 'auctions-' . $auction->getKey(),
                'tag'  => 'banner',
            ]);

            $image->uploaded_file = $file;
            $auction->banner()->save($image);
        }

        return $this;
    }

    /**
     * Prepare the address and location attribute to be
     * in an acceptable format by the auction.
     *
     * @param array $data
     * @return array
     */
    protected function prepareLocation(array $data): array
    {
        $address = $data['location']['address'];
        $location = (new Location())
            ->setLatitude($data['location']['latitude'])
            ->setLongitude($data['location']['longitude']);

        return compact('address', 'location');
    }
}