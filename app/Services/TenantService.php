<?php
/**
 * Created by PhpStorm.
 * User: harry
 * Date: 4/12/18
 * Time: 1:03 AM
 */

namespace App\Services;


use App\Events\TenantConfirmed;
use App\Events\TenantCreated;
use App\Models\System\Tenant;
use App\Models\Tenant\User;
use Illuminate\Support\Collection;

class TenantService
{
    /**
     * Create a tenant and its first user, the Super Administrator.
     *
     * @param array $data
     * @return Collection
     */
    public function create(array $data): Collection
    {
        // First we create the tenant record on the system schema
        // and connect to its schema...
        $tenant = Tenant::create($data['tenant']);

        // Then we create a new user on the tenant data.
        // The first user of any tenant is a super-admin, ever.
        $user = User::createSuperAdmin($tenant, array_only($data, ['name', 'email', 'password']));

        event(new TenantCreated($tenant, $user));

        return collect([
            'user'   => $user,
            'tenant' => $tenant,
        ]);
    }

    /**
     * Confirm the Tenant registration.
     *
     * @param Tenant $tenant
     * @return bool
     */
    public function confirmRegistration(Tenant $tenant)
    {
        if (!$tenant->hasStatus($tenant::STATUS_PENDING_CONFIRMATION)) {
            return false;
        }

        $status = $tenant->update(['status' => $tenant::STATUS_PENDING_DOMAIN]);

        if ((bool)$status) {
            event(new TenantConfirmed($tenant));
        }

        return $status;
    }

    /**
     * Update the tenant domain.
     *
     * @param Tenant $tenant
     * @param string $domain
     * @return bool
     */
    public function updateDomain(Tenant $tenant, string $domain): bool
    {
        $input = compact('domain');

        if ($tenant->hasStatus($tenant::STATUS_PENDING_DOMAIN)) {
            $input['status'] = $tenant::STATUS_ACTIVE;
        }

        $tenant->update($input);

        return true;
    }
}