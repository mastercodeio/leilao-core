<?php
/**
 * Created by PhpStorm.
 * User: harry
 * Date: 13/07/2018
 * Time: 03:01
 */

namespace App\Services;


use App\Models\System\Tenant;
use App\Models\Tenant\Auction;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\UploadedFile;

interface AuctionServiceContract
{
    /**
     * Define the validation rules to be used on creation.
     *
     * @return array
     */
    public function rulesCreate(): array;

    /**
     * Define the validation rules to be used on update.
     *
     * @return array
     */
    public function rulesUpdate(): array;

    /**
     * Create a new auction entity.
     *
     * @param array  $data
     * @param Tenant $tenant
     * @return Model
     */
    public function create(Tenant $tenant, array $data): Model;

    /**
     * Update an existing auction.
     *
     * @param Auction $item
     * @param array   $data
     * @return Auction
     */
    public function update(Auction $item, array $data): Auction;

    /**
     * Update information of the specified auction.
     *
     * @param Auction $item
     * @param array   $data
     * @return $this
     */
    public function updateAuctionInfo(Auction $item, array $data);

    /**
     * Update phases of the specified auction.
     *
     * @param Auction $item
     * @param array   $data
     * @return $this
     */
    public function updateAuctionPhases(Auction $item, array $data);

    /**
     * Update an auction logo.
     *
     * @param Auction      $auction
     * @param UploadedFile $file
     * @param bool         $removePrevious
     * @return $this
     */
    public function updateAuctionLogo(Auction $auction, ?UploadedFile $file, bool $removePrevious = false);

    /**
     * Update an auction banner.
     *
     * @param Auction      $auction
     * @param UploadedFile $file
     * @param bool         $removePrevious
     * @return $this
     */
    public function updateAuctionBanner(Auction $auction, ?UploadedFile $file, bool $removePrevious = false);
}