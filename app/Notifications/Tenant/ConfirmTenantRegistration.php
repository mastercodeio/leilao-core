<?php

namespace App\Notifications\Tenant;

use App\Models\System\Tenant;
use Illuminate\Bus\Queueable;
use Illuminate\Notifications\Messages\MailMessage;
use Illuminate\Notifications\Notification;
use Illuminate\Support\Facades\URL;

class ConfirmTenantRegistration extends Notification
{
    use Queueable;
    /**
     * @var Tenant
     */
    private $tenant;

    /**
     * Create a new notification instance.
     *
     * @param Tenant $tenant
     */
    public function __construct(Tenant $tenant)
    {
        $this->tenant = $tenant;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return ['mail'];
    }

    /**
     * Get the mail representation of the notification.
     *
     * @param  mixed $notifiable
     * @return \Illuminate\Notifications\Messages\MailMessage
     */
    public function toMail($notifiable)
    {
        return (new MailMessage)
            ->greeting("Seja bem vindo, {$notifiable->name}!")
            ->line('Para iniciar, você só precisa confirmar seu cadastro e preparar o seu domínio para a MeuLeilão.')
            ->action('Continuar cadastro', URL::signedRoute('site.account.confirm', ['tenant' => $this->tenant]))
            ->line('Obrigado!');
    }

    /**
     * Get the array representation of the notification.
     *
     * @param  mixed $notifiable
     * @return array
     */
    public function toArray($notifiable)
    {
        return [
            //
        ];
    }
}
