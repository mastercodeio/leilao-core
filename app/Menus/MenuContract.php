<?php
/**
 * Created by PhpStorm.
 * User: harry
 * Date: 31/05/18
 * Time: 21:29
 */

namespace App\Menus;

interface MenuContract
{
    /**
     * List of groups and items on the menu.
     *
     * @return array
     */
    public function items(): array;
}