<?php
/**
 * Created by PhpStorm.
 * User: harry
 * Date: 31/05/18
 * Time: 21:26
 */

namespace App\Menus;

class TenantMainMenu
{
    /**
     * @inheritdoc
     */
    public function items(): array
    {
        return [
            'Principal' => [
                'Dashboard' => route('tenant.main.dashboard'),
            ],
            'Leilões'   => [
                'Leilões ativos'      => route('tenant.main.auctions.index'),
                'Categorias de Lotes' => route('tenant.main.categories.index'),
            ],
            'Usuários'  => [
                'Usuários aprovados' => '#',
                'Usuários pendentes' => '#',
                'Adicionar usuário'  => '#',
            ],
        ];
    }
}