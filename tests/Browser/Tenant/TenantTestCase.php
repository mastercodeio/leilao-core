<?php
/**
 * Created by PhpStorm.
 * User: harry
 * Date: 31/05/18
 * Time: 22:57
 */

namespace Tests\Browser\Tenant;

use App\Services\TenantService;
use Tests\DuskTestCase;

class TenantTestCase extends DuskTestCase
{
    protected $tenant;
    protected $masterUser;

    /**
     * @param bool $confirmed
     * @return mixed
     */
    protected function createTenant(bool $confirmed = true)
    {
        $data = [
            'tenant'   => [
                'registration_code' => 'RC001',
                'name'              => 'Test Tenant',
            ],
            'name'     => 'John Doe',
            'email'    => 'john@doe.com',
            'password' => 'asdasdasd',
        ];

        $tenantService = app(TenantService::class);
        $result = $tenantService->create($data);

        $tenantService->updateDomain($result->get('tenant'), 'leilao-cliente.test');

        if ($confirmed) {
            $tenantService->confirmRegistration($result->get('tenant'));
        }

        $this->tenant = $result->get('tenant');
        $this->masterUser = $result->get('user');
    }

    /**
     * @param      $url
     * @param null $params
     * @param null $secure
     * @return string
     */
    protected function tenantUrl($url, $params = null, $secure = null): string
    {
        return url(config('app.protocol') . '://' . $this->tenant->domain . $url, $params, $secure);
    }

    protected function tenantLogin($browser)
    {
        $browser->visit($this->tenantUrl('/escritorio/entrar'))
            ->assertSee($this->tenant->name)
            ->type('email', $this->masterUser->email)
            ->type('password', 'asdasdasd')
            ->press('Entrar');
    }
}