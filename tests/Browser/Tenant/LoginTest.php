<?php

namespace Tests\Browser\Tenant;

use Laravel\Dusk\Browser;

class LoginTest extends TenantTestCase
{
    /**
     * @throws \Throwable
     */
    public function testLogin()
    {
        $this->createTenant();

        $this->browse(function (Browser $browser) {
            $browser->visit($this->tenantUrl('/escritorio/entrar'))
                ->assertSee($this->tenant->name)
                ->type('email', $this->masterUser->email)
                ->type('password', 'asdasdasd')
                ->press('Entrar')
                ->assertRouteIs('tenant.main.dashboard');
        });
    }
}
