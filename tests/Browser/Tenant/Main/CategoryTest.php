<?php

namespace Tests\Browser\Tenant\Main;

use Laravel\Dusk\Browser;
use Tests\Browser\Tenant\TenantTestCase;

class CategoryTest extends TenantTestCase
{
    /**
     * @throws \Throwable
     */
    public function testIndexPage()
    {
        $this->createTenant();

        $this->browse(function (Browser $browser) {
            $this->tenantLogin($browser);

            $browser->visit($this->tenantUrl('/escritorio/categorias'))
                ->assertSee('Categorias de Lote')
                ->assertVisible('#list');
        });
    }
}
