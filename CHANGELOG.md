# Changelog

All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](http://keepachangelog.com/en/1.0.0/)
and this project adheres to [Semantic Versioning](http://semver.org/spec/v2.0.0.html).

## [Unreleased]
### Added
- Package to generate Entity-Relationship Diagrams from Models.

### Changed
- Database migrations were refactored. Migrations must be refreshed. 

## [0.1.0] - 2018-07-07
### Added
- All code created until the date was not documented on a changelog. This one marks the beginning of Changelog.