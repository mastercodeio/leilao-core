<?php

/**
 * Tenant routes.
 *
 * Tenants use their own domain, so we have to catch all routes inside
 * if
 */

Route::get('/media/{filename}', 'FileController@stream')->name('files.stream')->where('filename', '^(.+)\/([^\/]+)$');

// Authentication
Route::get('/escritorio/entrar', 'LoginController@showLoginForm')->name('login.form');
Route::post('/escritorio/entrar', 'LoginController@login')->name('login.attempt');
Route::post('/escritorio/sair', 'LoginController@logout')->name('logout');

// Auctioneer Admin
Route::prefix('/escritorio')->middleware('auth:tenant')->group(function () {
    Route::get('/', 'Main\DashboardController@index')->name('main.dashboard');

    Route::get('/leiloes', 'Main\AuctionController@index')->name('main.auctions.index');
    Route::get('/leiloes/adicionar', 'Main\AuctionController@create')->name('main.auctions.create');
    Route::post('/leiloes/adicionar', 'Main\AuctionController@store')->name('main.auctions.store');
    Route::get('/leiloes/{item}', 'Main\AuctionController@edit')->name('main.auctions.edit');
    Route::patch('/leiloes/{item}', 'Main\AuctionController@update')->name('main.auctions.update');
    Route::delete('/leiloes/{item}', 'Main\AuctionController@destroy')->name('main.auctions.destroy');

    Route::get('/categorias', 'Main\CategoryController@index')->name('main.categories.index');
    Route::get('/categorias/adicionar', 'Main\CategoryController@create')->name('main.categories.create');
    Route::post('/categorias/adicionar', 'Main\CategoryController@store')->name('main.categories.store');
    Route::get('/categorias/{category}', 'Main\CategoryController@edit')->name('main.categories.edit');
    Route::patch('/categorias/{category}', 'Main\CategoryController@update')->name('main.categories.update');
    Route::delete('/categorias/{category}', 'Main\CategoryController@destroy')->name('main.categories.destroy');
});

// Auctioneer Site
Route::as('site.')->group(function () {
    Route::get('/', function () {
        return 'SITE';
    });
});