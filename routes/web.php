<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'HomeController@index')->name('home');

// Registration routes ...
Route::get('/cadastro', 'RegisterController@showBaseForm')->name('register.form');
Route::post('/cadastro', 'RegisterController@storeBaseData')->name('register.store');
Route::get('/cadastro/bem-vindo', 'RegisterController@welcome')->name('register.welcome');

// Authentication routes ...
Route::get('/login', 'LoginController@showLoginForm')->name('login.form');
Route::post('/login', 'LoginController@login')->name('login.attempt');
Route::post('/logout', 'LoginController@logout')->name('logout');

// Confirmation of registering ...
Route::get('/conta/{tenant}/confirmacao', 'AccountController@confirm')->name('account.confirm');

// Account routes ...
Route::prefix('/conta')->as('account.')->middleware('auth:site')->group(function () {
    Route::get('/', 'AccountController@index')->name('index');
    Route::patch('/dominio', 'AccountController@updateDomain')->name('domain.update');
});

