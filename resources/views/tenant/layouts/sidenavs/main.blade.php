<aside class="menu mt-3">
    @foreach ($sidenav_menu->items() as $title => $config)
        @if (is_array($config))
            @component('tenant.layouts.sidenavs.partials.group', ['items' => $config])
                {{ $title }}
            @endcomponent
        @endif
    @endforeach
</aside>