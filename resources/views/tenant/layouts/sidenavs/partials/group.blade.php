<p class="menu--label">{{ $slot }}</p>
<ul class="menu--list">
    @foreach ($items as $label => $url)
        @component('tenant.layouts.sidenavs.partials.item', ['url' => $url])
            {{ $label }}
        @endcomponent
    @endforeach
</ul>