<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ app('tenant')->name }}</title>

    <!-- Styles -->
    <link href="{{ asset('tenant/css/app.css') }}" rel="stylesheet">
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.0.13/css/all.css" integrity="sha384-DNOHZ68U8hZfKXOrtjWvjxusGo9WQnrNx2sqG0tfsghAvtVlRW3tvkXWZh58N9jp" crossorigin="anonymous">

    <!-- Scripts -->
    <script src="https://maps.googleapis.com/maps/api/js?key={{ config('services.google.places') }}&libraries=places&language=pt-br"></script>
    <script defer src="{{ asset('tenant/js/app.js') }}"></script>
</head>
<body class="bg-grey-lightest  @stack('body-class')">
<div id="app">
    @include('tenant.layouts.partials.navbar')

    <section class="flex flex-col sm:flex-row container mx-auto my-4">
        <div class="w-full sm:w-1/3 p-2">
            @include('tenant.layouts.sidenavs.' . $nav)
        </div>
        <div class="w-full p-2">
            <nav class="py-2 mb-2" aria-label="breadcrumbs">
                <ul class="breadcrumb">
                    <li>
                        @if ($nav === 'main')
                            <a href="{{ route('tenant.main.dashboard') }}">Principal</a>
                        @elseif ($nav === 'cash-flow')
                            <a href="#">Fluxo de Caixa</a>
                        @elseif ($nav === 'content')
                            <a href="#">Conteúdo</a>
                        @elseif ($nav === 'account')
                            <a href="#">Minha Conta</a>
                        @endif
                    </li>
                    @yield('breadcrumbs')
                </ul>
            </nav>

            @yield('content')
        </div>
    </section>

    <div class="w-full text-center flex flex-row justify-center">
        @if (session('success'))
            <notification type="success" :popup="true">
                {!! session('success') !!}
            </notification>
        @endif
        @if ($errors->has('email'))
            <notification type="warning" :popup="true">
                {{ $errors->first('email') }}
            </notification>
        @endif
    </div>
</div>
</body>
</html>