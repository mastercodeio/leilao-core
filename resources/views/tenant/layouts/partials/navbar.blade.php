<section class="nav">
    <nav class="container flex flex-col text-center py-2 md:flex-row md:justify-between md:items-center">
        <div class="py-2">
            <a href="{{ route('tenant.main.dashboard') }}" class="nav--item nav--item__logo">
                {{ $tenant->name }}
            </a>
        </div>
        <div class="py-2 flex-grow text-center md:text-left md:border-l md:border-brand-light md:ml-6">
            <a href="{{ route('tenant.main.dashboard') }}" class="nav--item">Principal</a>
            <a href="#" class="nav--item">Fluxo de Caixa</a>
            <a href="#" class="nav--item">Conteúdo</a>
        </div>
        <div class="py-2">
            <a href="#" class="nav--item">Minha Conta</a>
            <a class="nav--item" href="{{ route('tenant.logout') }}" onclick="event.preventDefault();document.getElementById('logout-form').submit();">Sair</a>
            {{ html()->form('POST', route('tenant.logout'))->id('logout-form')->open() }} {{ html()->form()->close() }}
        </div>
    </nav>
</section>