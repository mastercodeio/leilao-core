@extends('tenant.layouts.master', ['nav' => 'main'])

@section('breadcrumbs')
    @tenantBreadcrumb(['link' => route('tenant.main.auctions.index')]) Leiões @endtenantBreadcrumb
    @tenantBreadcrumb(['link' => '#', 'active' => true]) {{ $page_title }} @endtenantBreadcrumb
@endsection

@section('content')
    <auction-form-ctrl :phases="{{ $item->phases->toJson() }}">
        <template slot="content" slot-scope="props">
            <div class="flex justify-between items-center mb-4">
                <h1 class="title">{{ $page_title }}</h1>
            </div>

            <h2 class="text-sm text-grey-dark">HASHID {{ $item->hashid }}</h2>

            {{ html()->modelForm($item, $item->exists ? 'PATCH' : 'POST', $item->url('save'))->acceptsFiles()->class('pt-4')->open() }}
            <div class="card">
                <h2 class="card__title">Geral</h2>

                <div class="field">
                    <label class="field__label">Identificador</label>
                    {{ html()->text('identifier', old('identifier'))->placeholder('Identificador')->class('field__input')->required() }}
                    @if ($errors->has('identifier'))
                        <p class="text-red text-xs italic">{{ $errors->first('identifier') }}</p>
                    @endif
                </div>

                <div class="field">
                    <label class="field__label">Título</label>
                    {{ html()->text('title', old('title'))->placeholder('Título')->class('field__input')->required() }}
                    @if ($errors->has('title'))
                        <p class="text-red text-xs italic">{{ $errors->first('title') }}</p>
                    @endif
                </div>

                <div class="field">
                    <label class="field__label">Descrição</label>
                    {{ html()->textarea('description', old('description'))->attribute('rows', 5)->placeholder('Descrição')->class('field__input') }}
                    @if ($errors->has('description'))
                        <p class="text-red text-xs italic">{{ $errors->first('description') }}</p>
                    @endif
                </div>

                <div class="field w-full  pl-2">
                    <label class="field__label">Localização </label>
                    <google-places
                            name="location"
                            address="{{ $item->address or '' }}"
                            latitude="{{ $item->location->getLatitude() }}"
                            longitude="{{ $item->location->getLongitude() }}"
                            :required="true">
                    </google-places>
                    @if ($errors->has('location'))
                        <p class="text-red text-xs italic">{{ $errors->first('location') }}</p>
                    @endif
                </div>
            </div>

            <div class="card mt-6">
                <h2 class="card__title">Praças</h2>

                <div class="flex items-center" v-for="(phase, index) in props.formData.phases">
                    <input type="hidden" :name="'phases[' +  index + '][id]'" :value="phase.id">
                    <div class="field w-1/2 pr-2">
                        <label class="field__label">Data</label>
                        <datepicker class="field__input" format="dd/MM/yyyy" :name="'phases[' +  index + '][date]'" v-model="phase.date"></datepicker>
                    </div>
                    <div class="field flex-grow px-2">
                        <label class="field__label">Tipos</label>
                        <input v-for="type in phase.types" type="hidden" :name="'phases[' + index + '][types][]'" :value="type.value">
                        <multi-select class="field__input"
                                      v-model="phase.types"
                                      :options="{{ \App\Models\Tenant\Embeds\Type::getSelectOptions()->toJson() }}"
                                      track-by="value"
                                      label="label"
                                      :allow-empty="false"
                                      :close-on-select="false"
                                      :multiple="true">
                        </multi-select>
                    </div>
                    <div class="flex-shrink pl-2">
                        <button type="button" class="button button-secondary" @click.prevent="props.removePhase(index)">X</button>
                    </div>
                </div>
            </div>

            <div class="card mt-6">
                <h2 class="card__title">Imagens</h2>

                <div class="flex flex-col md:flex-row">
                    <div class="field w-full md:w-1/2 pr-2 text-center">
                        <input type="hidden" name="logo_remove" :value="props.formData.removeLogo">
                        <label class="field__label">Logo</label>
                        <picture-input name="logo"
                                       value="{{ $item->logo ? $item->logo->url : '' }}"
                                       prefill="{{ $item->logo ? $item->logo->url : '' }}"
                                       width="300"
                                       height="300"
                                       margin="16"
                                       accept="image/jpeg,image/png"
                                       size="10"
                                       :custom-strings="{upload: '<p>Seu dispositivo não suporta envio de arquivos.</p>',drag: 'Arraste uma imagem <br>ou clique aqui', change: 'Alterar Imagem', remove: 'Remover Imagem', select: 'Selecionar Imagem'}"
                                       :removable="true"
                                       button-class="button"
                                       @remove="props.formData.removeLogo = 1"
                                       remove-button-class="button button--secondary">
                        </picture-input>

                        @if ($errors->has('logo'))
                            <p class="text-red text-xs italic">{{ $errors->first('logo') }}</p>
                        @endif
                    </div>
                    <div class="field w-full md:w-1/2 pl-2 text-center">
                        <input type="hidden" name="banner_remove" :value="props.formData.removeBanner">
                        <label class="field__label">Banner</label>
                        <picture-input name="banner"
                                       prefill="{{ $item->banner ? $item->banner->url : '' }}"
                                       width="300"
                                       height="300"
                                       margin="16"
                                       accept="image/jpeg,image/png"
                                       size="10"
                                       :custom-strings="{upload: '<p>Seu dispositivo não suporta envio de arquivos.</p>',drag: 'Arraste uma imagem <br>ou clique aqui', change: 'Alterar Imagem', remove: 'Remover Imagem', select: 'Selecionar Imagem'}"
                                       :removable="true"
                                       button-class="button"
                                       @remove="props.formData.removeBanner = 1"
                                       remove-button-class="button button--secondary">
                        </picture-input>

                        @if ($errors->has('banner'))
                            <p class="text-red text-xs italic">{{ $errors->first('banner') }}</p>
                        @endif
                    </div>
                </div>
            </div>

            <div class="card my-6 text-right">
                <button class="button button--lg">Salvar</button>
            </div>

            {{ html()->closeModelForm() }}
        </template>
    </auction-form-ctrl>
@endsection
