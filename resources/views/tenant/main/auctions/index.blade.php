@extends('tenant.layouts.master', ['nav' => 'main'])

@section('breadcrumbs')
    @tenantBreadcrumb(['link' => '#']) Leilões @endtenantBreadcrumb
    @tenantBreadcrumb(['link' => route('tenant.main.auctions.index'), 'active' => true]) {{ $table_title }} @endtenantBreadcrumb
@endsection

@section('content')
    <div class="flex justify-between items-center mb-4">
        <h1 class="title">Leilões</h1>
        <a href="{{ \App\Models\Tenant\Auction::urlCreate() }}" class="button">Adicionar</a>
    </div>
    <div class="card">
        <h2 class="card__title">{{ $table_title }}</h2>
        <table class="table w-full">
            <thead>
            <th width="120">#</th>
            <th>Nome</th>
            <th>Data</th>
            <th width="120">Ações</th>
            </thead>
            <tbody>
            @forelse($items as $item)
                <tr>
                    <td>{{ $item->identifier }}</td>
                    <td>{{ $item->title }}</td>
                    <td>{{ $item->date }}</td>
                    <td>@component('tenant.components.table-actions', ['item' => $item]) @endcomponent</td>
                    @empty
                        <td :colspan="5" class="has-text-centered">
                            <span class="inline-block notification">Nenhum leilão cadastrado.</span>
                        </td>
                </tr>
            @endforelse
            </tbody>
            <tfoot>
            <th>#</th>
            <th>Nome</th>
            <th>Data</th>
            <th>Ações</th>
            </tfoot>
        </table>
    </div>
@endsection
