@extends('tenant.layouts.master', ['nav' => 'main'])

@section('breadcrumbs')
    @tenantBreadcrumb(['link' => '#', 'active' => true]) Dashboard @endtenantBreadcrumb
@endsection

@section('content')
    <div class="bg-brand p-4 shadow-md rounded leading-normal">
        <h1 class="text-white text-5xl font-bold">
            Olá, {{ auth()->user()->first_name }}!
        </h1>
        <h2 class="text-white text-lg font-light mb-2">
            Você não tem notificações por enquanto.
        </h2>
    </div>
@endsection
