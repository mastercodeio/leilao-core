@extends('tenant.layouts.master', ['nav' => 'main'])

@section('breadcrumbs')
    @tenantBreadcrumb(['link' => route('tenant.main.auctions.index')]) Leilões @endtenantBreadcrumb
    @tenantBreadcrumb(['link' => route('tenant.main.categories.index')]) Categorias de Lote @endtenantBreadcrumb
    @tenantBreadcrumb(['link' => '#', 'active' => true]) {{ $title }} @endtenantBreadcrumb
@endsection

@section('content')
    <div class="flex justify-between items-center mb-4">
        <h1 class="title">Adicionar Categorias de Lote</h1>
    </div>
    <div class="card">
        <h2 class="card__title">Geral</h2>
        {{ html()->modelForm($item, $item->getKey() ? 'PATCH' : 'POST', $item->url('form'))->open() }}

        @component('tenant.components.field', ['class' => 'w-full', 'error' => 'title'])
            @slot('label', 'Nome da Categoria')
            {{ html()->text('title')->placeholder('Nome')->class('field__input')->required() }}
        @endcomponent

        <div class="text-right">
            <a href="{{ route('tenant.main.categories.index') }}" class="button button--secondary">Cancelar</a>
            <button type="submit" class="button">Salvar</button>
        </div>

        {{ html()->form()->close() }}
    </div>
@endsection
