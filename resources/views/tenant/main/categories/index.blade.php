@extends('tenant.layouts.master', ['nav' => 'main'])

@section('breadcrumbs')
    @tenantBreadcrumb(['link' => route('tenant.main.auctions.index')]) Leilões @endtenantBreadcrumb
    @tenantBreadcrumb(['link' => route('tenant.main.categories.index'), 'active' => true]) Categorias de Lote @endtenantBreadcrumb
@endsection

@section('content')
    <div class="flex justify-between items-center mb-4">
        <h1 class="title">Categorias de Lote</h1>
        <a href="{{ route('tenant.main.categories.create') }}" class="button">Adicionar</a>
    </div>
    <div class="card">
        <h2 class="card__title">Categorias</h2>
        <table id="list" class="table w-full">
            <thead>
            <th>Nome</th>
            <th width="120">Ações</th>
            </thead>
            <tbody>
            @forelse($items as $item)
                <tr>
                    <td>{{ $item->title }}</td>
                    <td>@component('tenant.components.table-actions', ['item' => $item]) @endcomponent</td>
                    @empty
                        <td :colspan="5" class="has-text-centered">
                            <div class="notification">Nenhuma categoria de leilão cadastrada.</div>
                        </td>
                </tr>
            @endforelse
            </tbody>
            <tfoot>
            <th>Nome</th>
            <th>Ações</th>
            </tfoot>
        </table>
    </div>
@endsection
