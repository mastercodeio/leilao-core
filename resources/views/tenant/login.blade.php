@extends('tenant.layouts.minimal')

@push('body-class', 'pg-login bg-brand-dark')

@section('content')
    <div class="h-screen w-full flex flex-col items-center justify-center">
        <div class="w-full md:w-1/4 p-8 mb-8 bg-white shadow-md border-t-8 border-brand rounded">
            <div class="w-full mb-8 text-center">
                <h1 class="font-light text-3xl text-brand-darkest">{{ app('tenant')->name }}</h1>
            </div>
            <div class="w-full mb-8 my-2">
                <div class="w-full">
                    @if (session('success'))
                        <notification type="success">
                            {!! session('success') !!}
                        </notification>
                    @endif
                    @if ($errors->has('email'))
                        <notification type="warning">
                            {{ $errors->first('email') }}
                        </notification>
                    @endif
                </div>
            </div>
            {{ html()->form('POST', route('tenant.login.attempt'))->open() }}
            <div class="w-full field">
                <label class="field__label">
                    Email
                </label>
                {{ html()->email('email', old('email'))->placeholder('Seu email')->class('field__input')->required() }}
            </div>
            <div class="w-full field">
                <label class="field__label">
                    Senha
                </label>
                {{ html()->password('password')->placeholder('Senha')->class('field__input')->required() }}
            </div>
            <div class="w-full">
                <button class="button button--lg w-full">Entrar</button>
            </div>
            {{ html()->form()->close() }}

            <div class="w-full mt-8 text-base font-normal text-center">
                <a class="text-grey-dark" href="#">Esqueceu a senha?</a>
            </div>
        </div>
    </div>
@endsection