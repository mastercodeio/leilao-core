<span class="{{ $class }}">
    <i class="{{ $icon }}"></i> {{ $slot }}
</span>
