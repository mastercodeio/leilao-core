<div class="field {{ $class or '' }}">
    <label class="field__label">{{ $label }}</label>

    {{ $slot }}

    @if ($errors->has($error))
        <p class="field__error">{{ $errors->first($error) }}</p>
    @endif
</div>