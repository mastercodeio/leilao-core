@php
    $id = 'table-actions-' . $item->getKey();
@endphp

<dropdown>
    @if (!isset($hideEdit))
        @component('tenant.components.dropdown-item', ['url' => $item->url('edit')])
            <i class="fas fa-pencil-alt"></i> Editar
        @endcomponent
    @endif
    @if (!isset($hideDelete))
        @component('tenant.components.dropdown-item', ['url' => $item->url('destroy')])
            <i class="fas fa-trash-alt"></i> Remover
        @endcomponent
    @endif
</dropdown>