<li>
    <a href="{{ $link }}" @if (isset($active)) aria-current="page" @endif>{{ $slot }}</a>
</li>