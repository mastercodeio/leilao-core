<li>
    <a href="{{ $url or '#' }}" class="px-4 py-2 block no-underline text-sm text-black hover:no-underline hover:bg-grey-light">{{ $slot }}</a>
</li>