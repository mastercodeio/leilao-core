@extends('site.layouts.master')

@push('body-class', 'pg-register-welcome')

@section('content')
    <div class="h-screen w-full flex flex-col items-center justify-center bg-brand">
        <div class="p-8 text-center">
            <h1 class="text-5xl mb-8 font-bold text-white">Olá, {{ $user_name }}!</h1>
            <h2 class="text-3xl mb-8 font-bold text-white">Seja bem-vindo à {{ config('app.name') }}</h2>
            <p class="text-xl text-white leading-normal">Enviamos ao seu e-mail as instruções para finalizar seu cadastro. <br>Qualquer dúvida entre em contato conosco!</p>
        </div>
    </div>
@endsection