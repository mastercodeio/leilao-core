@extends('site.layouts.master')

@push('body-class', 'pg-register bg-brand-lightest')

@section('content')
    <section class="container mx-auto pt-16 mb-8 text-center">
        <h1 class="font-light text-3xl">{{ config('app.name') }}</h1>
        <p class="font-light text-xl py-4">Por favor preencha seus dados para prosseguir com o cadastro.</p>
    </section>

    {{ html()->form('POST', route('site.register.store'))->class('container w-2/3 mx-auto py-6 text-left bg-white shadow border-t-8 border-brand rounded')->open() }}
    <div class="mx-3 md:flex mb-6">
        <div class="md:w-1/2 px-3 mb-6 md:mb-0 field">
            <label class="field__label">
                Nome da sua empresa
            </label>
            {{ html()->text('tenant[name]', old('tenant.name'))->placeholder('Nome da sua empresa')->class('field__input')->required() }}
            @if ($errors->has('tenant.name'))
                <p class="text-red text-xs italic">{{ $errors->first('tenant.name') }}</p>
            @endif
        </div>
        <div class="md:w-1/2 px-3 field">
            <label class="field__label">
                Registro na Junta Comercial
            </label>
            {{ html()->text('tenant[registration_code]', old('tenant.registration_code'))->placeholder('Registro na Junta Comercial')->class('field__input')->required() }}
            @if ($errors->has('tenant.registration_code'))
                <p class="text-red text-xs italic">{{ $errors->first('tenant.registration_code') }}</p>
            @endif
        </div>
    </div>
    <div class="mx-3 md:flex mb-6">
        <div class="md:w-1/2 px-3 mb-6 md:mb-0 field">
            <label class="field__label">Seu nome</label>
            {{ html()->text('name', old('name'))->placeholder('Seu nome')->class('field__input')->required() }}
            @if ($errors->has('name'))
                <p class="text-red text-xs italic">{{ $errors->first('name') }}</p>
            @endif
        </div>
        <div class="md:w-1/2 px-3 field">
            <label class="field__label">
                Seu email
            </label>
            {{ html()->email('email', old('email'))->placeholder('Seu email')->class('field__input')->required() }}
            @if ($errors->has('email'))
                <p class="text-red text-xs italic">{{ $errors->first('email') }}</p>
            @endif
        </div>
    </div>
    <div class="mx-3 md:flex mb-6">
        <div class="md:w-1/2 px-3 mb-6 md:mb-0 field">
            <label class="field__label">
                Sua senha
            </label>
            {{ html()->password('password')->placeholder('Senha')->class('field__input')->required() }}
            @if ($errors->has('password'))
                <p class="text-red text-xs italic">{{ $errors->first('password') }}</p>
            @endif
        </div>
        <div class="md:w-1/2 px-3 field">
            <label class="field__label">
                Confirme sua senha
            </label>
            {{ html()->password('password_confirmation')->placeholder('Confirmaçao da senha')->class('field__input')->required() }}
            @if ($errors->has('password_confirmation'))
                <p class="text-red text-xs italic">{{ $errors->first('password_confirmation') }}</p>
            @endif
        </div>
    </div>

    <div class="mb-6 text-center">
        <button class="button button--lg">Cadastrar</button>
    </div>

    {{ html()->form()->close() }}
@endsection