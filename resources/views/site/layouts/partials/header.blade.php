<section class="bg-brand w-full p-2">
    <div class="text-center text-white pb-8">
        <div>
            {{ $head or '' }}
        </div>
        <div class="py-32">
            {{ $slot }}
        </div>
    </div>
</section>