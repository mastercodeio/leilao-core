<footer class="flex justify-center py-8 mt-6 text-center">
    <p class="text-grey-dark text-sm">
        Um produto <a href="https://www.mastercode.com.br" class="font-bold">MasterCode</a>. Todos os direitos reservados.
    </p>
</footer>