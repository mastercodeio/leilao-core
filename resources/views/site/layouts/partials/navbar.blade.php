<section class="flex justify-center bg-white border-b border-grey-lighter">
    <nav class="container flex flex-col text-center py-2 px-8 md:flex-row md:justify-between md:items-center">
        <div class="py-2">
            <a href="{{ route('site.home') }}" class="font-bold text-xl text-grey-darkest no-underline hover:text-black">{{ config('app.name') }}</a>
        </div>
        <div class="py-2">
            @if (auth()->guard('site')->check())
                <a href="{{ route('site.home') }}" class="font-bold text-base text-grey-darkest no-underline px-6 py-4 hover:text-brand">Home</a>
                <a href="{{ route('site.account.index') }}" class="font-bold text-base text-grey-darkest no-underline px-6 py-4 hover:text-brand">Minha Conta</a>
                <a href="{{ route('site.logout') }}" class="font-bold text-base text-grey-darkest no-underline px-6 py-4 hover:text-brand"
                   onclick="event.preventDefault();document.getElementById('logout-form').submit();">
                    Sair
                </a>
                {{ html()->form('POST', route('site.logout'))->id('logout-form')->open() }} {{ html()->form()->close() }}
            @else
                <a href="{{ route('site.home') }}" class="font-bold text-base text-grey-darkest no-underline px-4 py-4 hover:text-brand">Home</a>
                <a href="{{ route('site.login.form') }}" class="font-bold text-base text-grey-darkest no-underline px-4 py-4 hover:text-brand">Entrar</a>
                <a href="{{ route('site.register.form') }}" class="button">
                    <i class="fas fa-bullseye"></i> Cadastrar-se
                </a>
            @endif
        </div>
    </nav>
</section>