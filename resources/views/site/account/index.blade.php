@extends('site.layouts.master')

@push('body-class', 'pg-account bg-grey-lightest')

@section('content')
    <div class="container mx-auto pb-8">
        @if (session('success'))
            <notification type="success">
                {!! session('success') !!}
            </notification>
        @endif
        @if (session('error'))
            <notification type="warning">
                {!! session('error') !!}
            </notification>
        @endif
        @if (session('info'))
            <notification type="info">
                {!! session('info') !!}
            </notification>
        @endif
    </div>

    <div class="container mx-auto my-8 p-6 bg-brand rounded shadow text-left">
        <h1 class="text-5xl text-white font-light">Olá {{ auth()->user()->first_name }}!</h1>
    </div>

    <div class="container mx-auto text-right mb-8">
        @if ($tenant->hasDomainSetup())
            <a href="{{ $tenant->route('tenant.main.dashboard') }}" class="button">Acessar Escritório</a>
        @endif
    </div>

    <div class="container mx-auto mb-8 p-6 bg-white rounded shadow border-t-8 border-brand">
        <domain-settings
                domain="{{ $tenant->domain }}"
                mode="{{ $tenant->hasDomainSetup() ? 'update' : 'add' }}"
                action="{{ route('site.account.domain.update') }}"
                csrf="{{ csrf_token() }}">
        </domain-settings>
    </div>
@endsection