@extends('site.layouts.master')

@push('body-class', 'pg-home')

@section('content')
    @component('site.layouts.partials.header')
        <div class="container mx-auto text-center">
            <h1 class="font-light text-3xl">
                <strong class="font-bold">Dou-lhe uma, Dou-lhe duas, vendido</strong> <br>ficou muito mais fácil!
            </h1>
            <p class="font-light text-xl leading-normal">
                <br>
                A plataforma {{ config('app.name') }} permite a venda de lotes presenciais e online, de forma simultânea, sem espera, sem estresse, e com exatamente o que você precisa para finalizar
                um arremate.
            </p>
        </div>
    @endcomponent

    <div class="w-full bg-white py-6 shadow">
        <div class="container mx-auto text-center">
            <span class="tag">Novidade</span> Agora você pode permitir que o arrematante realize o pagamento pela plataforma com cartão de crédito ou boleto bancário.
        </div>
    </div>

    <section class="container mx-auto mt-8 flex justify-center">
        <div class="w-full md:w-1/3 p-4">
            <div class="bg-white rounded overflow-hidden shadow">
                <div class="px-6 py-4">
                    <div class="font-bold text-xl mb-2">The Coldest Sunset</div>
                    <p class="text-grey-darker text-base">
                        Lorem ipsum dolor sit amet, consectetur adipisicing elit. Voluptatibus quia, nulla! Maiores et perferendis eaque, exercitationem praesentium nihil.
                    </p>
                </div>
                <div class="px-6 py-4">
                    <span class="inline-block bg-grey-lighter rounded-full px-3 py-1 text-sm font-semibold text-grey-darker mr-2">#photography</span>
                    <span class="inline-block bg-grey-lighter rounded-full px-3 py-1 text-sm font-semibold text-grey-darker mr-2">#travel</span>
                    <span class="inline-block bg-grey-lighter rounded-full px-3 py-1 text-sm font-semibold text-grey-darker">#winter</span>
                </div>
            </div>
        </div>
        <div class="w-full md:w-1/3 p-4">
            <div class="bg-white rounded overflow-hidden shadow">
                <div class="px-6 py-4">
                    <div class="font-bold text-xl mb-2">The Coldest Sunset</div>
                    <p class="text-grey-darker text-base">
                        Lorem ipsum dolor sit amet, consectetur adipisicing elit. Voluptatibus quia, nulla! Maiores et perferendis eaque, exercitationem praesentium nihil.
                    </p>
                </div>
                <div class="px-6 py-4">
                    <span class="inline-block bg-grey-lighter rounded-full px-3 py-1 text-sm font-semibold text-grey-darker mr-2">#photography</span>
                    <span class="inline-block bg-grey-lighter rounded-full px-3 py-1 text-sm font-semibold text-grey-darker mr-2">#travel</span>
                    <span class="inline-block bg-grey-lighter rounded-full px-3 py-1 text-sm font-semibold text-grey-darker">#winter</span>
                </div>
            </div>
        </div>
        <div class="w-full md:w-1/3 p-4">
            <div class="bg-white rounded overflow-hidden shadow">
                <div class="px-6 py-4">
                    <div class="font-bold text-xl mb-2">The Coldest Sunset</div>
                    <p class="text-grey-darker text-base">
                        Lorem ipsum dolor sit amet, consectetur adipisicing elit. Voluptatibus quia, nulla! Maiores et perferendis eaque, exercitationem praesentium nihil.
                    </p>
                </div>
                <div class="px-6 py-4">
                    <span class="inline-block bg-grey-lighter rounded-full px-3 py-1 text-sm font-semibold text-grey-darker mr-2">#photography</span>
                    <span class="inline-block bg-grey-lighter rounded-full px-3 py-1 text-sm font-semibold text-grey-darker mr-2">#travel</span>
                    <span class="inline-block bg-grey-lighter rounded-full px-3 py-1 text-sm font-semibold text-grey-darker">#winter</span>
                </div>
            </div>
        </div>
    </section>
@endsection