import { Vue } from '../../common/js/app.js';
import Notification from './components/notification';
import Modal from './components/modal';
import DomainSettings from './account/domain-settings';

/** Generic Components */
Vue.component('notification', Notification);
Vue.component('modal', Modal);

/** Specific Components */
Vue.component('domain-settings', DomainSettings);