import axios from 'axios';

const api = axios.create();

/**
 * We'll load the axios HTTP library which allows us to easily issue requests
 * to our Laravel back-end. This library automatically handles sending the
 * CSRF token as a header based on the value of the "XSRF" token cookie.
 */

api.defaults.headers.common['X-Requested-With'] = 'XMLHttpRequest';

/**
 * Next we will register the CSRF Token as a common header with Axios so that
 * all outgoing HTTP requests automatically have it attached. This is just
 * a simple convenience so we don't have to attach every token manually.
 */

api.interceptors.request.use((config) => {
    let token = document.head.querySelector('meta[name="csrf-token"]');

    if (token) {
        config.headers.common['X-CSRF-TOKEN'] = token.content;
    }
});

export default api;