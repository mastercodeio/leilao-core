import Vue from 'vue';
import Turbolinks from 'turbolinks';
import TurbolinksAdapter from 'vue-turbolinks';

Turbolinks.start();
Vue.use(TurbolinksAdapter);

const initVue = () => {
    new Vue({
        el: '#app',
    });
}

document.addEventListener('turbolinks:load', () => {
    initVue();
})

export { Vue };