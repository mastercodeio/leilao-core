import { Vue } from '../../common/js/app.js';
import Datepicker from 'vuejs-datepicker';
import PictureInput from 'vue-picture-input';
import Notification from './components/notification';
import Modal from './components/modal';
import Dropdown from './components/dropdown';
import GooglePlaces from './components/google-places';
import CustomSelect from './components/custom-select';
import MultiSelect from 'vue-multiselect';

import AuctionForm from './controllers/auction-form';

/** Generic Components */
Vue.component('notification', Notification);
Vue.component('modal', Modal);
Vue.component('dropdown', Dropdown);
Vue.component('google-places', GooglePlaces);
Vue.component('custom-select', CustomSelect);
Vue.component('datepicker', Datepicker);
Vue.component('picture-input', PictureInput);
Vue.component('multi-select', MultiSelect);

Vue.component('auction-form-ctrl', AuctionForm);